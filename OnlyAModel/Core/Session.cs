﻿using System.Net.Sockets;
using Microsoft.Extensions.Logging;
using OnlyAModel.Util;

namespace OnlyAModel.Core;

/// <summary>
/// A big ugly bag of state.
/// </summary>
public sealed partial class Session(Dispatcher dispatcher, Sender sender, ILogger<Session> log) : IDisposable
{
	private static readonly IdPool IdPool = new();
	private readonly IdPool.Token _sessionId = IdPool.Rent();
	public ushort SessionId => _sessionId.Id;

	public async Task RunAsync(Socket socket, CancellationToken serverCancel)
	{
		_socket.Init(socket);
		_sessionCancel.Init(CancellationTokenSource.CreateLinkedTokenSource(serverCancel));
		log.LogInformation("[{id}] Session started for {ep}.", SessionId, Socket.RemoteEndPoint);
		try
		{
			await ReadSocketAsync();
		}
		catch (Exception e)
		{
			if (e is not OperationCanceledException)
			{
				log.LogError(e, "Session error.");
			}
		}
		finally
		{
			Socket.Close();
		}
	}

	public async Task KickAsync()
	{
		await _sessionCancel.Value.CancelAsync();
	}

	public void Dispose()
	{
		ExitRegion();
		_sessionId.Dispose();
		_socket.Dispose();
		_sessionCancel.Dispose();
	}
}