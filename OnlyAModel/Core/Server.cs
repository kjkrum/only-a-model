﻿using Microsoft.Extensions.Logging;
using OnlyAModel.Util;

namespace OnlyAModel.Core;

/// <summary>
/// Main class and entry point.
/// </summary>
public sealed class Server(Listener listener, Sender sender, ILogger<Server> log) : IDisposable
{
	private readonly Deferred<CancellationTokenSource> _serverCancel = new();
	private CancellationToken ServerCancel => _serverCancel.Value.Token;
	
	public async Task RunAsync(CancellationToken externalCancel)
	{
		log.LogInformation($"{nameof(Server)} starting.");
		_serverCancel.Init(CancellationTokenSource.CreateLinkedTokenSource(externalCancel));
		await Task.WhenAny(
			Task.Run(async () => await sender.RunAsync(ServerCancel), ServerCancel),
			Task.Run(async () => await listener.RunAsync(ServerCancel), ServerCancel));
		await _serverCancel.Value.CancelAsync();
		log.LogInformation($"{nameof(Server)} stopping.");
	}
	
	public void Dispose()
	{
		_serverCancel.Dispose();
	}
}