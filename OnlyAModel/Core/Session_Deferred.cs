﻿using System.Net.Sockets;
using OnlyAModel.Protocol.Client.Models;
using OnlyAModel.Util;

namespace OnlyAModel.Core;

public partial class Session
{
	/* set by RunAsync */
	private readonly Deferred<Socket> _socket = new();
	private Socket Socket => _socket.Value;
	
	private readonly Deferred<CancellationTokenSource> _sessionCancel = new();
	public CancellationToken SessionCancel => _sessionCancel.Value.Token;
	
	/* set by handshake */
	private readonly DeferredValue<ClientInfo> _clientInfo = new();
	public ClientInfo ClientInfo => _clientInfo.Value;
	private readonly DeferredValue<int> _protocolVersion = new();
	public int ProtocolVersion => _protocolVersion.Value;

	public void SetClientInfo(ClientInfo clientInfo)
	{
		_clientInfo.Init(clientInfo);
		_protocolVersion.Init(clientInfo.ProtocolVersion);
	}

	/* set by login */
	private readonly DeferredValue<int> _playerId = new();
	public int PlayerId => _playerId.Value;

	public void SetPlayerId(int playerId) => _playerId.Init(playerId);
}