﻿namespace OnlyAModel.Core.GameObjects;

public static class Actions
{
	// see notes in PlayerCharacter
	public const byte Wireframe = 0x01;
	public const byte Stealthed = 0x02;
	public const byte Diving = 0x04;
	public const byte GroundTargetInView = 0x08;
	public const byte TargetInView = 0x30;
	public const byte Torch = 0x80;
}