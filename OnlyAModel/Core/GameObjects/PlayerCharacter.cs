﻿using OnlyAModel.Data.Entities;
using OnlyAModel.Protocol.Server;
using OnlyAModel.Protocol.Server.Messages;

namespace OnlyAModel.Core.GameObjects;

public record PlayerCharacter : GameObject
{
	// TODO exclude mutable session and character from equality
	
	public PlayerCharacter(Session session, Character character, ushort objectId)
	{
		Session = session;
		Character = character;
		RegionId = character.RegionId;
		X = character.X;
		Y = character.Y;
		Z = character.Z;
		Heading = character.Heading;
		ObjectId = objectId;
	}

	private Session Session { get; init; }
	private Character Character { get; init; }
	public ushort SessionId => Session.SessionId;
	public float Speed { get; init; }
	public float ZSpeed { get; init; }
	public ushort ZoneId { get; init; }

	/// <summary>
	/// Includes wireframe, stealth, diving, torch, and targets.
	/// </summary>
	public byte Action { get; init; }
	public bool Wireframe => (Action & Actions.Wireframe) != 0;
	public bool Stealthed => (Action & Actions.Stealthed) != 0;
	public bool Diving => (Action & Actions.Diving) != 0;
	public bool GroundTargetInView => (Action & Actions.GroundTargetInView) != 0;
	// TODO test if one of the TargetInView bits means target friendly
	public bool TargetInView => (Action & Actions.TargetInView) == Actions.TargetInView;
	public bool Torch => (Action & Actions.Torch) != 0;

	/// <summary>
	/// Includes jumping, swimming, sitting, climbing, and strafing.
	/// </summary>
	public byte State { get; init; }
	// Climbing is apparently Jumping + Swimming + Sitting
	public bool Jumping => (State & States.Climbing) == States.Jumping;
	public bool Swimming => (State & States.Climbing) == States.Swimming;
	public bool Sitting => (State & States.Climbing) == States.Sitting;
	public bool Climbing => (State & States.Climbing) == States.Climbing;
	// strafing left: 0x60 while holding key, followed by one 0x40
	// strafing right: 0xA0 while holding key, followed by one 0x80
	// theory: 0x40 means left, 0x80 means right, 0x20 means strafing
	// when we get 0x80 or 0x40 without 0x20, it's a positive confirmation
	// of stopping
	public bool StrafingLeft => (State & 0x60) == 0x60;
	public bool StrafingRight => (State & 0xA0) == 0xA0;
	public bool Strafing => (State & 0x20) == 0x20;
	public byte DamageFlags { get; init; }
	
	/// <summary>
	/// An object ID; zero for none.
	/// </summary>
	public ushort TargetId { get; init; }
	public bool Attacking { get; init; }

	public byte HealthPercent => 100; // TODO
	public byte ManaPercent => 100; // TODO
	public byte EndurancePercent => 100; // TODO

	/* abstract GameObject members */
	
	public override bool Aware => true;
	
	public override bool Detectable => true;

	public override bool CanDetect(GameObject gameObject) => true; // TODO real stuff

	public override void OnEnterAwareness(GameObject gameObject) =>
		Session.Send(gameObject.GetEnterAwarenessMessages());
	
	public override void OnUpdatePosition(GameObject gameObject) =>
		Session.Send(gameObject.GetUpdatePositionMessage());
	
	public override void OnUpdateHeading(GameObject gameObject) =>
		Session.Send(gameObject.GetUpdateHeadingMessage());
	
	public override void OnExitAwareness(GameObject gameObject) =>
		Session.Send(gameObject.GetExitAwarenessMessage());
	
	public override void OnRemoveFromWorld(GameObject gameObject) =>
		Session.Send(gameObject.GetRemoveFromWorldMessage());
	
	public override IEnumerable<ServerMessage> GetEnterAwarenessMessages() =>
		new List<ServerMessage>()
		{
			new PlayerCreate(SessionId, ObjectId, Character),
			new EquipmentUpdate(ObjectId, Speed)
		};

	public override ServerMessage GetUpdatePositionMessage() => new PlayerPosition(this);
	
	public override ServerMessage GetUpdateHeadingMessage()
	{
		throw new NotImplementedException(); // TODO
	}

	public override ServerMessage GetExitAwarenessMessage() => new ObjectOutOfRange(ObjectId);

	public override ServerMessage GetRemoveFromWorldMessage() => new RemoveFromWorld(ObjectId);
}