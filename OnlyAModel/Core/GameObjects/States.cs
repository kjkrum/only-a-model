﻿namespace OnlyAModel.Core.GameObjects;

public static class States
{
	// see notes in PlayerCharacter
	public const byte Swimming = 0x04;
	public const byte Jumping = 0x08;
	public const byte Sitting = 0x10;
	// the idea that 0x10 is sitting was based on how another client rendered
	// climbing with jumping and swimming masked off
	// TODO implement /sit and test this
	public const byte Climbing = Swimming + Jumping + Sitting;
	public const byte Strafing = 0x20;
	public const byte StrafingLeft = 0x60;
	public const byte StrafingRight = 0xA0;
}