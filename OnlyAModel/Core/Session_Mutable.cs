﻿using OnlyAModel.Core.GameObjects;
using OnlyAModel.Data.Entities;
using OnlyAModel.Util;

namespace OnlyAModel.Core;

#nullable disable // these properties are non-null during gameplay

public partial class Session
{
	// How we manage the Character's presence in a Region is dictated by the
	// stupidity of the protocol. The client sends the following messages when
	// you click "Play":
	//
	// RegionRequest
	// GameOpenRequest
	// WorldInitRequest
	// CharacterInitRequest
	//
	// We must initialize the Region in order to respond to the RegionRequest.
	// We must rent an object ID from the Region in order to respond to the
	// WorldInitRequest. But we cannot add the PlayerCharacter to the Region
	// until we respond to the CharacterInitRequest or the client will not
	// recognize object creation messages and will spam RefreshPlayerRequest
	// when it receives a PlayerPositionUpdate for a player it didn't create.
	
	public Character Character { get; set; }
	public Region Region { get; set; }
	public IdPool.Token IdToken { get; set; }
	public PlayerCharacter PlayerCharacter { get; set; }

	public void ExitRegion()
	{
		if (PlayerCharacter != null)
		{
			_ = Region?.TryRemove(PlayerCharacter);
			PlayerCharacter = null;
		}
		IdToken?.Dispose();
		IdToken = null;
		Region = null;
	}
}