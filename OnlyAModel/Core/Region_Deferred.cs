﻿using Microsoft.Extensions.Logging;
using OnlyAModel.Extensions;
using OnlyAModel.Spatial;
using OnlyAModel.Util;

namespace OnlyAModel.Core;

public partial class Region
{
	private readonly Deferred<Data.Entities.Region> _regionInfo = new();
	public ushort RegionId => _regionInfo.Value.RegionId;
	public string Name => _regionInfo.Value.Name;
	private int AwarenessRadius => _regionInfo.Value.AwarenessRadius;
	private readonly Deferred<Tree<GameObject>> _tree = new();
	private Tree<GameObject> Tree => _tree.Value;
	
	private const double GoldenRatio = 1.61803398874989484820458683436;
	private const float MagicMultiple = 2; // TODO tweak
	
	public void SetRegionInfo(Data.Entities.Region regionInfo)
	{
		_regionInfo.Init(regionInfo);
		var bounds = regionInfo.ComputeBounds();
		// We want square-ish leaf nodes. Minimum depth is the number of times
		// the aspect ratio must be halved before it is less than or equal to
		// the golden ratio. I don't think there are any long, narrow regions
		// so this is probably always zero or one. Depth should be this minimum
		// plus an even number that produces node sizes some multiple of the
		// awareness radius.
		var depth = 0;
		var ratio = bounds.AspectRatio;
		while (ratio > GoldenRatio)
		{
			depth += 1;
			ratio /= 2;
		}
		var shortSide = Math.Min(bounds.Width, bounds.Height);
		if (ratio < 1) shortSide *= ratio;
		while (shortSide > regionInfo.AwarenessRadius * MagicMultiple)
		{
			depth += 2;
			shortSide /= 2;
		}
		_tree.Init(new Tree<GameObject>(bounds, depth));
		log.LogDebug("Initialized region {name} ({id}) to {w}x{h}, radius {r}, leaf size {s}, depth {d}.",
			Name, RegionId, bounds.Width, bounds.Height, AwarenessRadius, shortSide, depth);
	}
}