﻿using Microsoft.Extensions.DependencyInjection;

namespace OnlyAModel.Core;

public class SessionFactory(IServiceProvider sp)
{
	public Session CreateSession() => sp.GetRequiredService<Session>();
}