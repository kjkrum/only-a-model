﻿using System.Net;
using System.Net.Sockets;
using Microsoft.Extensions.Logging;

namespace OnlyAModel.Core;

/// <summary>
/// Listens for client connections.
/// </summary>
public sealed class Listener(Config config, SessionFactory sessionFactory, ILogger<Listener> log)
{
	public async Task RunAsync(CancellationToken serverCancel) =>
		await Task.WhenAny(config.Bindings.Select(binding =>
			Task.Run(async () => await ListenAsync(binding, serverCancel), serverCancel)));
	
	private async Task ListenAsync(Config.Binding binding, CancellationToken serverCancel)
	{
		try
		{
			var address = IPAddress.Parse(binding.Address);
			var serverSocket = new Socket(address.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
			var endPoint = new IPEndPoint(address, binding.Port);
			serverSocket.Bind(endPoint);
			serverSocket.Listen();
			log.LogInformation("Listening on {ep}.", endPoint);
			while (!serverCancel.IsCancellationRequested)
			{
				try
				{
					// https://stackoverflow.com/questions/76955978/which-socket-accept-errors-are-fatal
					var clientSocket = await serverSocket.AcceptAsync(serverCancel);
					_ = Task.Run(async () =>
					{
						using var session = sessionFactory.CreateSession();
						await session.RunAsync(clientSocket, serverCancel)
						 	.ConfigureAwait(ConfigureAwaitOptions.SuppressThrowing);
					}, serverCancel);
				}
				catch (Exception e)
				{
					if (e is not OperationCanceledException)
					{
						log.LogError(e, $"{nameof(Listener)} error.");
					}
				}
			}
		}
		catch (Exception e)
		{
			if (e is not OperationCanceledException)
			{
				log.LogCritical(e, $"{nameof(Listener)} error.");
			}
		}
	}
}