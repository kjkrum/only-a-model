﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.Extensions.Logging;
using OnlyAModel.Spatial;
using OnlyAModel.Util;

namespace OnlyAModel.Core;

public partial class Region(ILogger<Region> log)
{
	private readonly IdPool _objectIds = new();

	/// <summary>
	/// Logs a warning on failure.
	/// </summary>
	public bool TryRentObjectId([NotNullWhen(true)] out IdPool.Token? token)
	{
		var success = _objectIds.TryRent(out token);
		if (!success)
		{
			log.LogWarning("Failed to rent object ID for region {id}.", RegionId);
		}
		return success;
	}
	
	/// <summary>
	/// Logs a warning on failure.
	/// </summary>
	public bool TryAdd(GameObject obj)
	{
 		var entry = new Entry<GameObject>(obj.X, obj.Y, obj.Z, obj);
 		var query = Rectangle.Create(obj.X, obj.Y, AwarenessRadius);
	    if (!Tree.TryAdd(entry, query, out var result))
	    {
		    log.LogWarning("Failed to add {type} in region {id}.", obj.GetType().Name, RegionId);
		    return false;
	    }
	    // identify other objects in awareness radius
	    var others = result
			.Where(e => e.Value != obj && e.IsInSphere(entry, AwarenessRadius))
			.Select(e => e.Value)
			.ToArray();
	    // notify the added object about other objects
	    if (obj.Aware)
		{
			foreach (var other in others)
			{
				if (other.Detectable && obj.CanDetect(other))
				{
					obj.OnEnterAwareness(other);
				}
			}
		}
	    // notify other objects about the added object
	    // ReSharper disable once InvertIf
	    if (obj.Detectable)
		{
			foreach (var other in others)
			{
				if (other.Aware && other.CanDetect(obj))
				{
					other.OnEnterAwareness(obj);
				}
			}
		}
		// yatta!
		return true;
	}

	/// <summary>
	/// Logs a warning on failure.
	/// </summary>
	public bool TryRemove(GameObject obj)
	{
		var entry = new Entry<GameObject>(obj.X, obj.Y, obj.Z, obj);
		var query = Rectangle.Create(obj.X, obj.Y, AwarenessRadius);
		if (!Tree.TryRemove(entry, query, out var result))
		{
			log.LogWarning("Failed to remove {type} from region {id}.", obj.GetType().Name, RegionId);
			return false;
		}
		var others = result
			.Where(e => e.IsInSphere(entry, AwarenessRadius))
			.Select(e => e.Value)
			.ToArray();
		// ReSharper disable once InvertIf
		if (obj.Detectable)
		{
			foreach (var other in others)
			{
				if (other.Aware && other.CanDetect(obj))
				{
					other.OnRemoveFromWorld(obj);
				}
			}
		}
		return true;
	}
	
	// TODO should an update from inside to outside the region bounds be a remove, and the reverse an add?
	// this would handle the player jumping off the edge of the world at a region boundary
	// currently they will ghost where they fell off
	// could just kill them or something, so hold off on special handling here

	/// <summary>
	/// Logs a warning on failure.
	/// </summary>
	public bool TryUpdate(GameObject obj0, GameObject obj1)
	{
		if (obj0.GetType() != obj1.GetType() || obj0.ObjectId != obj1.ObjectId)
		{
			throw new ArgumentException("Arguments must have the same type and object ID.");
		}
		// there are separate server messages for player position and heading.
		// apparently there is only one kind of update for other game objects.
		// TODO detect heading-only or action-only updates?
		// these would be simpler but they're probably a small percentage of updates
		// need to know what the client sends when a player stealths
		// and whether action change may be combined with position or heading change
		// if not this, then eliminate GameObject OnUpdateHeading and GetHeadingUpdateMessage
		var entry0 = new Entry<GameObject>(obj0.X, obj0.Y, obj0.Z, obj0);
		var entry1 = new Entry<GameObject>(obj1.X, obj1.Y, obj1.Z, obj1);
		// query bounds include both positions
		var minX = Math.Min(obj0.X, obj1.X) - AwarenessRadius;
		var minY = Math.Min(obj0.Y, obj1.Y) - AwarenessRadius;
		var maxX = Math.Max(obj0.X, obj1.X) + AwarenessRadius;
		var maxY = Math.Max(obj0.Y, obj1.Y) + AwarenessRadius;
		var query = Rectangle.Create(minX, minY, maxX, maxY);
		if (!Tree.TryReplace(entry0, entry1, query, out var result))
		{
			log.LogWarning("Failed to update {type} in region {id}.", obj0.GetType().Name, RegionId);
			return false;
		}
		var nearby = result.Where(e => e.Value != obj1).ToArray();
		var nearby0 = nearby
			.Where(e => e.IsInSphere(entry0, AwarenessRadius))
			.Select(e => e.Value)
			.ToArray();
		var nearby1 = nearby
			.Where(e => e.IsInSphere(entry1, AwarenessRadius))
			.Select(e => e.Value)
			.ToArray();
		// notify updated object about nearby objects
		if (obj1.Aware)
		{
			// objects that obj0 could detect
			var canDetect0 = nearby0
				.Where(obj0.CanDetect)
				.ToArray();
			// objects that obj1 can detect
			var canDetect1 = nearby1
				.Where(obj1.CanDetect)
				.ToArray();
			// couldn't detect before, can detect now
			foreach (var other in canDetect1.Except(canDetect0))
			{
				obj1.OnEnterAwareness(other);
			}
			// could detect before, can't detect now
			foreach (var other in canDetect0.Except(canDetect1))
			{
				obj1.OnExitAwareness(other);
			}
		}
		// notify nearby objects about updated object
		if (!obj1.Detectable) return true;
		// could detect before
		foreach (var other in nearby0.Where(o => o.CanDetect(obj0)))
		{
			// can detect now
			if (nearby1.Contains(other) && other.CanDetect(obj1))
			{
				other.OnUpdatePosition(obj1);
			}
			// can't detect now
			else
			{
				other.OnExitAwareness(obj1);
			}
		}
		// couldn't detect before, can detect now
		foreach(var other in nearby1.Where(o => (!nearby0.Contains(o) || !o.CanDetect(obj0)) && o.CanDetect(obj1)))
		{
			other.OnEnterAwareness(obj1);
		}
		// couldn't detect before, can't detect now - nothing to do
		return true;
	}
}