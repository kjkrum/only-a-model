﻿using System.Collections.Immutable;
using Microsoft.Extensions.Logging;
using OnlyAModel.Protocol.Client;

namespace OnlyAModel.Core;

/// <summary>
/// Dispatches client messages to the correct handlers.
/// </summary>
public class Dispatcher
{
	private readonly ImmutableArray<IMessageHandler?> _handlers;
	private readonly ILogger<Dispatcher> _log;
	
	public Dispatcher(IEnumerable<IMessageHandler> handlers, ILogger<Dispatcher> log)
	{
		var array = new IMessageHandler?[256];
		foreach (var handler in handlers)
		{
			var type = (int) handler.Type;
			if (array[type] != null)
				throw new ArgumentException($"Multiple handlers for {handler.Type}.");
			array[type] = handler;
		}
		_handlers = array.ToImmutableArray();
		_log = log;
	}

	public async Task Dispatch(ClientMessage message, Session session)
	{
		var handler = _handlers[(int) message.Type];
		if (handler == null)
		{
			_log.LogWarning("No handler for {type}.", message.Type);
		}
		else
		{
			try
			{
				await handler.Handle(message, session);
			}
			catch (Exception e)
			{
				if (e is not OperationCanceledException)
				{
					_log.LogError(e, "Handler error.");
				}
			}
		}
	}
}