﻿namespace OnlyAModel.Core;

public interface IPosition
{
	public ushort RegionId { get; }
	public float X { get; }
	public float Y { get; }
	public float Z { get; }
	public ushort Heading { get; } // TODO could this be a Half?
}