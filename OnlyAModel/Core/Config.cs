﻿using System.ComponentModel.DataAnnotations;
using Microsoft.Extensions.Options;
using OnlyAModel.Protocol.Server.Models;

// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

namespace OnlyAModel.Core;

/// <summary>
/// Config binder.
/// </summary>
public class Config
{
	[Required, MinLength(1)]
	public string ServerName { get; init; } = null!;
	
	[Required, Range(1, 255)]
	public int ServerId { get; init; }
	
	[Required]
	public PvPMode PvPMode { get; init; }
	
	[Required, Range(1125, 1128)]
	public int MinProtocolVersion { get; init; }
	
	[Required, Range(1125, 1128)]
	public int MaxProtocolVersion { get; init; }
	
	[Required]
	public bool CreatePlayers { get; init; }
	
	[Required, ValidateEnumeratedItems]
	public IEnumerable<Binding> Bindings { get; init; } = null!;

	public class Binding
	{
		[Required]
		public string Address { get; init; } = null!;
		
		[Range(1, 65535)]
		public int Port { get; init; }
		
		// TODO external address and port, or "dynamic"
	}
}