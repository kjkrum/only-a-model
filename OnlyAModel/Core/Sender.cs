﻿using System.Threading.Channels;
using Microsoft.Extensions.Logging;
using OnlyAModel.Protocol.Server;

namespace OnlyAModel.Core;

/// <summary>
/// Sends server messages.
/// </summary>
public class Sender(ILogger<Sender> log)
{
	private record struct Entry(
		ServerMessage Message,
		Func<ServerMessage, Memory<byte>, Task> SendDelegate);

	// Ideally there would be a send queue for each network interface.
	// For a home server with one or two bindings, a single queue is fine.
	private readonly Channel<Entry> _channel = Channel.CreateUnbounded<Entry>();
	private readonly Memory<byte> _sendBuffer = new byte[2048];

	// TODO serialize the message here and enqueue the Memory<byte>?
	public bool Enqueue(ServerMessage message, Func<ServerMessage, Memory<byte>, Task> sendDelegate) =>
		_channel.Writer.TryWrite(new Entry(message, sendDelegate));
	
	public bool Enqueue(IEnumerable<ServerMessage> messages, Func<ServerMessage, Memory<byte>, Task> sendDelegate) =>
		messages.All(message => _channel.Writer.TryWrite(new Entry(message, sendDelegate)));

	public async Task RunAsync(CancellationToken serverCancel)
	{
		while (!serverCancel.IsCancellationRequested)
		{
			try
			{
				var entry = await _channel.Reader.ReadAsync(serverCancel);
				await entry.SendDelegate(entry.Message, _sendBuffer);
			}
			catch (Exception e)
			{
				if (e is not OperationCanceledException)
				{
					log.LogError(e, $"{nameof(Sender)} error.");
				}
			}
		}
	}
}