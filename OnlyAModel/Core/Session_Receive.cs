﻿using System.Buffers.Binary;
using System.Collections.Immutable;
using System.Net.Sockets;
using Microsoft.Extensions.Logging;
using OnlyAModel.Extensions;
using OnlyAModel.Protocol.Client;
using OnlyAModel.Util;

namespace OnlyAModel.Core;

public partial class Session
{
 	private readonly MemoryBuffer<byte> _readBuffer = new(new byte[2048]);

    /// <summary>
    /// Client message types that should not be logged.
    /// </summary>
    private static readonly ImmutableArray<ClientMessageType> SpammyClientMessages =
	    ImmutableArray.Create(
		    ClientMessageType.PlayerPositionUpdate,
		    ClientMessageType.PlayerHeadingUpdate,
		    ClientMessageType.Ping);
    
	private async Task ReadSocketAsync()
	{
		while (!SessionCancel.IsCancellationRequested)
		{
			var count = await Socket.ReceiveAsync(_readBuffer.Free, SocketFlags.None, SessionCancel);
			if (count == 0)
			{
				log.LogInformation("[{id}] Client disconnected.", SessionId);
				return;
			}
			_readBuffer.Append(count);
			while (TryReadMessage(out var message))
			{
				if (log.IsEnabled(LogLevel.Debug) && !SpammyClientMessages.Contains(message.Type))
				{
					log.LogDebug("[{id}] => [{code}] {type} (len={len})\n{hex}",
						SessionId, ((byte)message.Type).ToString("X2"), message.Type,
						message.Payload.Length, message.Payload.HexDump());	
				}
				await dispatcher.Dispatch(message, this);
			}
			_readBuffer.Compact();
		}	
	}

	private bool TryReadMessage(out ClientMessage message)
	{
		if (_readBuffer.Count > ClientMessage.EnvelopeBytes)
		{
			var len = ClientMessage.EnvelopeBytes +
			          BinaryPrimitives.ReadUInt16BigEndian(_readBuffer.Data.Span);
			if (_readBuffer.Count >= len)
			{
				message = new ClientMessage(_readBuffer.Data[..len]);
				_readBuffer.Advance(len);
				return true;
			}
		}
		message = default;
		return false;
	}
}