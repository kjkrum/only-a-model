﻿using System.Collections.Immutable;
using System.Net.Sockets;
using Microsoft.Extensions.Logging;
using OnlyAModel.Extensions;
using OnlyAModel.Protocol.Server;

namespace OnlyAModel.Core;

public partial class Session
{
	public void Send(ServerMessage message)
	{
		if (!SessionCancel.IsCancellationRequested)
		{
			sender.Enqueue(message, SendAsync);	// TODO use return value
		}
	}

	public void Send(IEnumerable<ServerMessage> messages)
	{
		if (!SessionCancel.IsCancellationRequested)
		{
			sender.Enqueue(messages, SendAsync); // TODO use return value
		}
	}

	/// <summary>
	/// Server message types that should not be logged.
	/// </summary>
	private static readonly ImmutableArray<ServerMessageType> SpammyServerMessages =
		ImmutableArray.Create(
			ServerMessageType.PlayerPosition,
			ServerMessageType.Pong);

	private async Task SendAsync(ServerMessage message, Memory<byte> sendBuffer)
	{
		if (!SessionCancel.IsCancellationRequested)
		{
			var data = message.Write(sendBuffer, ProtocolVersion);
			if (log.IsEnabled(LogLevel.Debug) && !SpammyServerMessages.Contains(message.Type))
			{
				var payload = data[3..];
				log.LogDebug("[{id}] <= [{code}] {type} (len={len})\n{hex}",
					SessionId, ((byte)message.Type).ToString("X2"),
					message.Type, payload.Length, payload.HexDump());	
			}
			try
			{
				await Socket.SendAsync(data, SocketFlags.None, SessionCancel);
			}
			catch (Exception)
			{
				// sender doesn't care
			}	
		}
	}
}