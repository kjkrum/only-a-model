﻿using OnlyAModel.Protocol.Server;

namespace OnlyAModel.Core;

public abstract record GameObject : IPosition
{
	// TODO record may not be appropriate for this - interface?
	
	public ushort RegionId { get; init; }
	public ushort ObjectId { get; init; }
	public float X { get; init; }
	public float Y { get; init; }
	public float Z { get; init; }
	public ushort Heading { get; init; }
	
	// considered making this an interface with C# 11 static properties
	// an interface with a static abstract property can't be used as a type parameter
	// a static virtual interface property can only be accessed via a type parameter
	
	/// <summary>
	/// True if this object is aware of other objects. This value must be
	/// constant for a given instance.
	/// </summary>
	public abstract bool Aware { get; }
	
	/// <summary>
	/// True if this object is ever detectable by other objects. This value
	/// must be constant for a given instance.
	/// </summary>
	public abstract bool Detectable { get; }
	
	/// <summary>
	/// True if this object can detect the specified object. Detection must be
	/// deterministic, not random. Detection may depend on this object's
	/// heading, but must not depend on the other object's heading.
	/// </summary>
	public abstract bool CanDetect(GameObject gameObject);
	
	// TODO properties to support CanDetect

	/// <summary>
	/// Called when another game object enters this object's awareness.
	/// </summary>
	public abstract void OnEnterAwareness(GameObject gameObject);
	
	/// <summary>
	/// Called when another game object's position is updated.
	/// </summary>
	public abstract void OnUpdatePosition(GameObject gameObject);
	
	// TODO there may be no compelling reason for the special case of a heading-only update
	public abstract void OnUpdateHeading(GameObject gameObject);
	
	/// <summary>
	/// Called when another game object exits this object's awareness.
	/// </summary>
	public abstract void OnExitAwareness(GameObject gameObject);
	
	/// <summary>
	/// Called when another game object is removed from the world.
	/// </summary>
	public abstract void OnRemoveFromWorld(GameObject gameObject);
	
	/// <summary>
	/// Gets the messages that should be sent to clients when this game object
	/// enters their awareness.
	/// </summary>
	public abstract IEnumerable<ServerMessage> GetEnterAwarenessMessages();
	
	/// <summary>
	/// Gets the message that should be sent to clients when this game object's
	/// position is updated.
	/// </summary>
	public abstract ServerMessage GetUpdatePositionMessage();
	
	/// <summary>
	/// Gets the message that should be sent to clients when this game object's
	/// heading is updated.
	/// </summary>
	public abstract ServerMessage GetUpdateHeadingMessage();
	
	/// <summary>
	/// Gets the message that should be sent to clients when this game object
	/// exits their awareness.
	/// </summary>
	public abstract ServerMessage GetExitAwarenessMessage();
	
	/// <summary>
	/// Gets the message that should be sent to clients when this game object
	/// is removed from the world.
	/// </summary>
	public abstract ServerMessage GetRemoveFromWorldMessage();
}