﻿using Microsoft.Extensions.DependencyInjection;

namespace OnlyAModel.Core;

public class RegionFactory(IServiceProvider sp)
{
	public Region CreateRegion() => sp.GetRequiredService<Region>();
}