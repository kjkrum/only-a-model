﻿using System.Collections.Concurrent;
using OnlyAModel.Data.Services;

namespace OnlyAModel.Core;

public class World(RegionService regionService, RegionFactory regionFactory)
{
	// TODO support instanced regions
	private readonly ConcurrentDictionary<int, Region> _regions = new();

	public async Task<Region?> GetRegionAsync(ushort regionId, CancellationToken cancel)
	{
		if (_regions.TryGetValue(regionId, out var existing))
		{
			return existing;
		}
		var regionInfo = await regionService.GetRegionAsync(regionId, cancel);
		if (regionInfo == null) return null;
		var created = regionFactory.CreateRegion();
		created.SetRegionInfo(regionInfo);
		return _regions.GetOrAdd(regionId, created);
	}
}