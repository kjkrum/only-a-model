﻿using System.Collections.Immutable;
using Microsoft.Extensions.Logging;
using OnlyAModel.Core;
using OnlyAModel.Protocol.Server;
using OnlyAModel.Protocol.Server.Models;

namespace OnlyAModel.Protocol.Client.Handlers;

public class SlashCommand : IMessageHandler
{
	private readonly ImmutableDictionary<string, ISlashCommand> _commands;

	public SlashCommand(IEnumerable<ISlashCommand> commands, ILogger<SlashCommand> log)
	{
		var d = new Dictionary<string, ISlashCommand>();
		foreach (var c in commands)
		{
			if (c.Aliases.Any())
			{
				foreach (var a in c.Aliases)
				{
					if (!d.TryAdd(a, c))
					{
						log.LogWarning("Command {name} has a duplicate alias {a}.", c.GetType().Name, a);			
					}
				}	
			}
			else
			{
				log.LogWarning("Command {name} has no aliases.", c.GetType().Name);				
			}
		}
		_commands = d.ToImmutableDictionary();
	}
	
	public ClientMessageType Type => ClientMessageType.SlashCommand;
	public async Task Handle(ClientMessage message, Session session)
	{
		var reader = new SpanReader(message.Payload.Span);
		reader.Skip(8);
		var args = reader.ReadCString()[1..].Split(); // remove the ampersand
		if (_commands.TryGetValue(args[0], out var cmd))
		{
			await cmd.Invoke(args, session);
		}
		else
		{
			session.Send(new ChatMessage($"Unknown command: {args[0]}", ChatType.System));
		}
	}
}