﻿using Microsoft.Extensions.Logging;
using OnlyAModel.Core;
using OnlyAModel.Data.Services;
using OnlyAModel.Protocol.Server.Messages;
using OnlyAModel.Protocol.Server.Models;

namespace OnlyAModel.Protocol.Client.Handlers;

public class LoginRequest(Config config, PlayerService players, ILogger<LoginRequest> log) : IMessageHandler
{
	public ClientMessageType Type => ClientMessageType.LoginRequest;
	public async Task Handle(ClientMessage message, Session session)
	{
		var reader = new SpanReader(message.Payload.Span);
		if (session.ProtocolVersion <= 1125)
		{
			reader.Skip(7); // redundant client info
		}
		var username = reader.ReadDAoCString();
		var password = reader.ReadDAoCString();

		var player = await players.GetPlayer(username, session.SessionCancel);
		if (player == null)
		{
			if (config.CreatePlayers)
			{
				player = await players.CreatePlayer(username, password, session.SessionCancel);
				if (player == null)
				{
					session.Send(new LoginDenied(LoginError.AccountNotFound));
					return;
				}
				log.LogInformation("[{sessionId}] Player {playerId} created.", session.SessionId, player.PlayerId);
				session.SetPlayerId(player.PlayerId);
				session.Send(new LoginGranted(username, config));
			}
			else
			{
				session.Send(new LoginDenied(LoginError.AccountNotFound));
			}
		}
		else if (!player.HasPassword(password))
		{
			session.Send(new LoginDenied(LoginError.PasswordIncorrect));
		}
		else if (player.Inactive)
		{
			session.Send(new LoginDenied(LoginError.AccountClosed));
		}
		// TODO check logged in
		else
		{
			log.LogInformation("[{sessionId}] Player {playerId} authenticated.", session.SessionId, player.PlayerId);
			session.SetPlayerId(player.PlayerId);
			session.Send(new LoginGranted(username, config));
		}
	}
}