﻿using OnlyAModel.Core;
using OnlyAModel.Core.GameObjects;
using OnlyAModel.Protocol.Server.Messages;

namespace OnlyAModel.Protocol.Client.Handlers;

public class WorldInitRequest : IMessageHandler
{
	public ClientMessageType Type => ClientMessageType.WorldInitRequest;
	
	public async Task Handle(ClientMessage message, Session session)
	{
		// DoL sends a ton of messages here, some multiple times
		// most of these are sent in WorldInitRequestHandler.cs
		// not sure where AddFriend and the last CharacterStatusUpdate come from

		// AddFriend - ???
		// PositionAndObjectId
		// Encumberance
		// MaxSpeed
		// MaxSpeed (again)
		// CharacterStatusUpdate
		// InventoryUpdate (equipment)
		// InventoryUpdate (inventory)
		// VariousUpdate (skills)
		// VariousUpdate (crafting skills)
		// DelveInfo times a million ("update player")
		// VariousUpdate (apparently part of "update player")
		// MoneyUpdate
		// StatsUpdate
		// VariousUpdate (resists, icons, weapon and armor stats)
		// QuestEntry
		// CharacterStatusUpdate (again)
		// CharacterPointsUpdate
		// Encumberance
		// ConcentrationList
		// CharacterStatusUpdate (again!)
		// ObjectGuildId
		// DebugMode
		// MaxSpeed (again!)
		// ControlledHorse

		if (session.Region.TryRentObjectId(out var token))
		{
			session.IdToken = token;
			session.PlayerCharacter = new PlayerCharacter(session, session.Character, token.Id);
			// PositionAndObjectId and DebugMode must be sent here
			session.Send(new PositionAndObjectId(session.PlayerCharacter));
			session.Send(new DebugMode(true));
		}
		else
		{
			await session.KickAsync();
		}
	}
}