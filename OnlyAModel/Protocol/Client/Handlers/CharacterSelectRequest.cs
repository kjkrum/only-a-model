﻿using OnlyAModel.Core;
using OnlyAModel.Protocol.Server.Messages;

namespace OnlyAModel.Protocol.Client.Handlers;

public class CharacterSelectRequest : IMessageHandler
{
	public ClientMessageType Type => ClientMessageType.CharacterSelectRequest;
	public Task Handle(ClientMessage message, Session session)
	{
		// According to DOL, the message format changed between 1.125c and
		// 1.125d and again in 1.126. 1.125d and lower send a character name,
		// but it's always "noname" unless (I'm guessing) the player uses
		// quick launch.
		
		// This shortcut works on 1.125d through 1.128c.
		session.Send(new SessionId(session.SessionId));
		return Task.CompletedTask;
	}
}