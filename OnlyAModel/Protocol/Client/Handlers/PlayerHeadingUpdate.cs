﻿using OnlyAModel.Core;
using OnlyAModel.Core.GameObjects;

namespace OnlyAModel.Protocol.Client.Handlers;

public class PlayerHeadingUpdate : IMessageHandler
{
	public ClientMessageType Type => ClientMessageType.PlayerHeadingUpdate;
	public async Task Handle(ClientMessage message, Session session)
	{
		var reader = new SpanReader(message.Payload.Span);
		reader.Skip(2); // session ID
		var heading = reader.ReadUInt16BE();
		reader.Skip(1);
		var action = reader.ReadByte();
		// 1.125d omits diving flag
		var diving = session.PlayerCharacter.Action & Actions.Diving;
		action += (byte)diving;
		// 1 byte SteedSlot
		// 1 byte RidingFlag
		// 1 byte health percent or something
		// 3 bytes unknown
		
		var pc = session.PlayerCharacter with
		{
			Heading = heading,
			Action = action
		};
		//log.LogDebug("{pc}", pc);
		if (session.Region.TryUpdate(session.PlayerCharacter, pc))
		{
			session.PlayerCharacter = pc;
		}
		else
		{
			await session.KickAsync();
		}
	}
}