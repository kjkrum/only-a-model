﻿using Microsoft.Extensions.Logging;
using OnlyAModel.Core;
using OnlyAModel.Data.Entities;
using OnlyAModel.Data.Enums;
using OnlyAModel.Data.Services;
using OnlyAModel.Protocol.Client.Models;

namespace OnlyAModel.Protocol.Client.Handlers;

public class CharacterOperationRequest(
	CharacterService characterService,
	ILogger<CharacterOperationRequest> log) : IMessageHandler
{
	public ClientMessageType Type => ClientMessageType.CharacterOperationRequest;
	public async Task Handle(ClientMessage message, Session session)
	{
		GetRequestType(message, out var operation, out var feature);
		switch (operation)
		{
			case CharacterOperation.Create:
				await characterService.CreateCharacterAsync(character =>
				{
					var reader = new SpanReader(message.Payload.Span);
					ReadCharacter(reader, character);
					SetDefaults(character);
					character.PlayerId = session.PlayerId;
				}, session.SessionCancel);
				break;
			case CharacterOperation.Delete:
				await characterService.DeleteCharacterAsync(
					session.PlayerId,
					message.Payload.Span[0],
					session.SessionCancel);
				break;
			case CharacterOperation.Customize:
				log.LogWarning("Customize not implemented.");
				break;
			default:
				log.LogWarning("[{id}] Bogus character operation: {op}.", session.SessionId, operation);
				break;
		}
		// TODO send character overview?
	}

	private static void GetRequestType(
		ClientMessage message,
		out CharacterOperation operation,
		out CharacterFeature feature)
	{
		// operation and feature are buried in the middle of the character data
		var reader = new SpanReader(message.Payload.Span);
		reader.Skip(1); // character slot
		_ = reader.ReadDAoCString(); // name
		reader.Skip(24); // various stuff
		operation = (CharacterOperation)reader.ReadByte();
		feature = (CharacterFeature)reader.ReadByte();
	}

	private static void ReadCharacter(SpanReader reader, Character character)
	{
		character.Slot = reader.ReadByte();
		character.Name = reader.ReadDAoCString();
		reader.Skip(4); // DOL notes suggest this is an array length
		character.CustomMode = reader.ReadByte();
		character.EyeSize = reader.ReadByte();
		character.LipSize = reader.ReadByte();
		character.EyeColor = reader.ReadByte();
		character.HairColor = reader.ReadByte();
		character.FaceType = reader.ReadByte();
		character.HairStyle = reader.ReadByte();
		reader.Skip(3);
		character.Mood = reader.ReadByte();
		reader.Skip(13); // 9 unknown, 1 operation, 1 feature, 2 unknown
		character.LocationDescription = reader.ReadDAoCString(); // location description
		_ = reader.ReadDAoCString(); // class name
		_ = reader.ReadDAoCString(); // race name
		character.Level = reader.ReadByte(); // according to Los Ojos
		character.Class = (Class)reader.ReadByte();
		character.Realm = (Realm)reader.ReadByte();
		var raceGender = reader.ReadByte();
		character.Race = (Race)(raceGender & 0x1F);
		character.Gender = (Gender)(raceGender & 0x80);
		character.Model = reader.ReadUInt16LE();
		character.RegionId = reader.ReadUInt16LE(); // Los Ojos reads byte, then skips 5
		reader.Skip(4); // unknown
		character.Strength = reader.ReadByte();
		character.Dexterity = reader.ReadByte();
		character.Constitution = reader.ReadByte();
		character.Quickness = reader.ReadByte();
		character.Intelligence = reader.ReadByte();
		character.Piety = reader.ReadByte();
		character.Empathy = reader.ReadByte();
		character.Charisma = reader.ReadByte();
	}

	private static void SetDefaults(Character character)
	{
		character.Level = 1;
		// TODO set starting location by race and class
		// this is cotswold
		character.RegionId = 1;
		character.X = 560467;
		character.Y = 511652;
		character.Z = 2344;
		character.Heading = 3398;
		// TODO real stats
		character.Health = 100;
		character.MaxHealth = 100;
		character.Mana = 100;
		character.MaxMana = 100;
		character.Endurance = 100;
		character.MaxEndurance = 100;
		character.Concentration = 100;
		character.MaxConcentration = 100;
	}
}