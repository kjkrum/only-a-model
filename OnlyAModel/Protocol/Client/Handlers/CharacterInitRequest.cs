﻿using OnlyAModel.Core;
using OnlyAModel.Protocol.Server.Messages;

namespace OnlyAModel.Protocol.Client.Handlers;

public class CharacterInitRequest : IMessageHandler
{
	public ClientMessageType Type => ClientMessageType.CharacterInitRequest;
	public async Task Handle(ClientMessage message, Session session)
	{
		session.Send(new CharacterInitFinished());
		if (!session.Region.TryAdd(session.PlayerCharacter))
		{
			await session.KickAsync();
		}
	}
}