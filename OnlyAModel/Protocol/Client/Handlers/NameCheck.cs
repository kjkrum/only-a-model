﻿using OnlyAModel.Core;
using OnlyAModel.Data.Services;
using OnlyAModel.Protocol.Server.Messages;
using OnlyAModel.Protocol.Server.Models;

namespace OnlyAModel.Protocol.Client.Handlers;

public class NameCheck(CharacterService characterService) : IMessageHandler
{
	public ClientMessageType Type => ClientMessageType.NameCheck;

	public async Task Handle(ClientMessage message, Session session)
	{
		var reader = new SpanReader(message.Payload.Span);
		// client imposes a length limit of 20, but the field width is larger
		var w = session.ProtocolVersion <= 1125 ? 30 : 24;
		var name = reader.ReadFixedString(w);
		// 1125 also sends username but we don't need it
		if (name == "noname")
		{
			session.Send(new NameCheckResponse(name, NameStatus.Prohibited));
		}
		else if (await characterService.NameExists(name, session.SessionCancel))
		{
			session.Send(new NameCheckResponse(name, NameStatus.Unavailable));
		}
		else
		{
			session.Send(new NameCheckResponse(name, NameStatus.Available));
		}
	}
}