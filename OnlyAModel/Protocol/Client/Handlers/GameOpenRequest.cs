﻿using OnlyAModel.Core;
using OnlyAModel.Protocol.Server.Messages;
using OnlyAModel.Protocol.Server.Models;

namespace OnlyAModel.Protocol.Client.Handlers;

public class GameOpenRequest : IMessageHandler
{
	public ClientMessageType Type => ClientMessageType.GameOpenRequest;
	public Task Handle(ClientMessage message, Session session)
	{
		var confirmUdp = message.Payload.Span[0];
		session.Send(new GameOpenResponse(confirmUdp));
		
		// 20 second delay here, as if the client is waiting for something
		// client still continues if we don't send any of these
		// TODO try sending more of the messages DOL sends for WorldInitRequest
		// at least some of these messages are effective here, as proven by the
		// last name and custom title from BogusPlayerUpdate
		
		// DelveInfo times a million (part of "update player")
		// VariousUpdate (resists, icons, weapon and armor stats)
		// VariousUpdate (skills)
		// VariousUpdate (crafting skills)
		// QuestEntry
		// ConcentrationList
		// ObjectGuildId
		// ControlledHorse
		
		// session.Send(new CharacterStatus(session.Character));
		// session.Send(new CharacterPoints(session.Character));
		// session.Send(new CharacterSpeed(200, 100, false));
		// session.Send(new Encumberance(100, 0));
		// session.Send(new BogusDisableSkills());
		// session.Send(new BogusMoneyUpdate());
		// session.Send(new BogusCharacterStats());
		// session.Send(new BogusCharacterResists());
		// session.Send(new BogusPlayerUpdate(session.Character));
		// session.Send(new BogusInventoryUpdate(InventoryType.Update));
		// session.Send(new BogusInventoryUpdate(InventoryType.Equipment));
		return Task.CompletedTask;
	}
}