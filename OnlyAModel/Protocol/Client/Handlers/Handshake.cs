﻿using System.Runtime.InteropServices;
using OnlyAModel.Core;
using OnlyAModel.Protocol.Client.Models;
using OnlyAModel.Protocol.Server.Messages;
using OnlyAModel.Protocol.Server.Models;

namespace OnlyAModel.Protocol.Client.Handlers;

public class Handshake(Config config) : IMessageHandler
{
	public ClientMessageType Type => ClientMessageType.Handshake;
	public async Task Handle(ClientMessage message, Session session)
	{
		if (message.Payload.Length > 7)
		{
			// Encryption key or something. Ignore it.
			return;
		}
		if (MemoryMarshal.TryRead(message.Payload.Span, out ClientInfo clientInfo))
		{
			session.SetClientInfo(clientInfo);
			var pv = clientInfo.ProtocolVersion;
			if (pv >= config.MinProtocolVersion && pv <= config.MaxProtocolVersion)
			{
				session.Send(new HandshakeResponse(clientInfo.Version, clientInfo.Build));
			}
			else
			{
				session.Send(new LoginDenied(LoginError.UnsupportedClientVersion));
			}
		}
		else
		{
			await session.KickAsync();
		}
	}
}