﻿using Microsoft.Extensions.Logging;
using OnlyAModel.Core;
using OnlyAModel.Data.Enums;
using OnlyAModel.Data.Services;
using OnlyAModel.Extensions;
using OnlyAModel.Protocol.Server.Messages;

namespace OnlyAModel.Protocol.Client.Handlers;

public class CharacterOverviewRequest(
	Config config,
	CharacterService characterService,
	ILogger<CharacterOverviewRequest> log) : IMessageHandler
{
	public ClientMessageType Type => ClientMessageType.CharacterOverviewRequest;
	public async Task Handle(ClientMessage message, Session session)
	{
		var realm = (Realm)message.Payload.Span[0];
		if (realm == Realm.None && config.PvPMode.AllowMultipleRealms())
		{
			// Special case that doesn't require loading characters.
			session.Send(new PlayerRealm(Realm.None));
		}
		var characters = (await characterService.GetCharactersAsync(session.PlayerId, session.SessionCancel)).ToList();
		var realms = characters.Select(c => c.Realm).ToList();
		if (realm == Realm.None)
		{
			// Client is inquiring which realm the player belongs to.
			switch (realms.Count)
			{
				case 0:
					session.Send(new PlayerRealm(Realm.None));
					break;
				case 1:
					session.Send(new PlayerRealm(realms[0]));
					break;
				default:
					log.LogWarning("Player {id} has characters in multiple realms.", session.PlayerId);
					await session.KickAsync();
					break;
			}
		}
		else
		{
			// Client is requesting the character list for a specific realm.
			if (config.PvPMode.AllowMultipleRealms())
			{
				session.Send(new CharacterOverview(characters.Where(c => c.Realm == realm), session.ProtocolVersion));
			}
			else if (realms.Count > 1)
			{
				log.LogWarning("Player {id} has characters in multiple realms.", session.PlayerId);
				await session.KickAsync();
			}
			else if (characters.Any(c => c.Realm != realm))
			{
				log.LogWarning("Player {id} sent a cross-realm character overview request.", session.PlayerId);
				await session.KickAsync();
			}
			else
			{
				session.Send(new CharacterOverview(characters, session.ProtocolVersion));
			}
		}
	}
}