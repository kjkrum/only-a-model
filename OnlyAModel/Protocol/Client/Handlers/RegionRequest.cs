﻿using Microsoft.Extensions.Logging;
using OnlyAModel.Core;
using OnlyAModel.Data.Services;
using OnlyAModel.Protocol.Server.Messages;

namespace OnlyAModel.Protocol.Client.Handlers;

public class RegionRequest(
	CharacterService characterService,
	World world,
	ILogger<RegionRequest> log) : IMessageHandler
{
	public ClientMessageType Type => ClientMessageType.RegionRequest;
	public async Task Handle(ClientMessage message, Session session)
	{
		/* 
		 * DOL sends ClientRegion (0xB1) with the selected character's region.
		 * LosOjos calls 0xB1 "StartArena" and only sends it when
		 * GameClient.Player is set, which it should always be after the
		 * preceding CharacterSelectRequest with a character name. If
		 * GameClient.Player is not set for some reason, LO sends ClientRegions
		 * (0x9E) with all regions.
		 */
		var slot = message.Payload.Span[0];
		var c = await characterService.GetCharacterAsync(session.PlayerId, slot, session.SessionCancel);
		if (c == null)
		{
			log.LogWarning("Invalid character slot {slot} for player {playerId}.", slot, session.PlayerId);
			await session.KickAsync();
			return;
		}
		var r = await world.GetRegionAsync(c.RegionId, session.SessionCancel);
		if (r == null)
		{
			log.LogWarning("Invalid region in character {characterId}.", c.CharacterId);
			await session.KickAsync();
			return;
		}
		session.Character = c;
		session.Region = r;
		session.Send(new CharacterRegion(r.RegionId, session.ClientInfo.Content.Expansions));
	}
}