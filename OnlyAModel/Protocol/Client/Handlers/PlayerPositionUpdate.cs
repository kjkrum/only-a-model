﻿using OnlyAModel.Core;
using OnlyAModel.Core.GameObjects;

namespace OnlyAModel.Protocol.Client.Handlers;

public class PlayerPositionUpdate : IMessageHandler
{
	public ClientMessageType Type => ClientMessageType.PlayerPositionUpdate;
	public async Task Handle(ClientMessage message, Session session)
	{
		// TODO send keepalive updates via region
		// 1.125d sends a position update every 2 seconds when idle. If other
		// clients don't receive this update, they'll assume the player is
		// linkdead and stop rendering them.
		
		// TODO test for bogus updates
		// my notes say there are random updates with X=0 Y=0, can't repro
		// also, updates may arrive after quit
		
		// TODO save character
		// save based on distance and time since last save
	
		var reader = new SpanReader(message.Payload.Span);
		var x = reader.ReadFloatLE();
		var y = reader.ReadFloatLE();
		var z = reader.ReadFloatLE();
		var speed = reader.ReadFloatLE();
		var zSpeed = reader.ReadFloatLE();
		reader.Skip(2); // session ID
		var zoneId = reader.ReadUInt16BE();
		var state = reader.ReadByte();
		reader.Skip(1);
		var damage = reader.ReadByte();
		reader.Skip(1);
		// masked because position update has e.g. 15 4b where heading update has 05 4b
		var heading = (ushort)(reader.ReadUInt16BE() & 0x0FFF);
		var action = reader.ReadByte();
		// 2 bytes unknown
		// 1 byte health percent or something
		// 2 bytes unknown
		
		// 1.125d swaps the diving and stealthed bits
		var diving = (action & Actions.Stealthed) << 1;
		var stealthed = (action & Actions.Diving) >> 1;
		action = (byte)((action & 0b11111001) + diving + stealthed);
		
		var pc = session.PlayerCharacter with
		{
			X = x,
			Y = y,
			Z = z,
			Speed = speed,
			ZSpeed = zSpeed,
			ZoneId = zoneId,
			State = state,
			Heading = heading,
			Action = action
		};
		//log.LogDebug("{pc}", pc);
		if (session.Region.TryUpdate(session.PlayerCharacter, pc))
		{ 
			session.PlayerCharacter = pc;
		}
		else
		{
			await session.KickAsync();
		}
	}
}