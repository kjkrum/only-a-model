﻿using OnlyAModel.Core;

namespace OnlyAModel.Protocol.Client.Handlers;

public class ClientExit : IMessageHandler
{
	public ClientMessageType Type => ClientMessageType.ClientExit;
	public async Task Handle(ClientMessage message, Session session)
	{
		// unknown content; DOL doesn't handle this at all
		// canceling the session avoids a SocketException in read
		await session.KickAsync();
	}
}