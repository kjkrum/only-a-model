﻿using OnlyAModel.Core;
using OnlyAModel.Protocol.Server.Messages;

namespace OnlyAModel.Protocol.Client.Handlers;

public class Ping : IMessageHandler
{
	public ClientMessageType Type => ClientMessageType.Ping;
	public Task Handle(ClientMessage message, Session session)
	{
		var reader = new SpanReader(message.Payload.Span);
		reader.Skip(4); // unknown
		/* Unit seems to be milliseconds. Epoch unknown. */
		var timestamp = reader.ReadUInt32BE();
		session.Send(new Pong(timestamp, message.Sequence));
		return Task.CompletedTask;
	}
}