﻿namespace OnlyAModel.Protocol.Client.Models;

/// <summary>
/// Associated with <see cref="CharacterOperation.Customize"/>.
/// </summary>
internal enum CharacterFeature : byte
{
	None = 0x00,
	Face = 0x01,
	Attributes = 0x02,
	Both = 0x03
}