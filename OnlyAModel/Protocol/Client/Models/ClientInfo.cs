﻿using System.Runtime.InteropServices;

// ReSharper disable UnassignedGetOnlyAutoProperty
// ReSharper disable MemberCanBePrivate.Global

namespace OnlyAModel.Protocol.Client.Models;

/// <summary>
/// Client version and capabilities.
/// </summary>
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public readonly struct ClientInfo
{
	public ClientContent Content { get; }
	public byte MajorVersion { get; }
	private readonly byte _minorVersion0;
	private readonly byte _minorVersion1;
	private readonly byte _revision;
	public ushort Build { get; }
	public int MinorVersion => _minorVersion0 * 100 + _minorVersion1;
	public char Revision => (char)_revision;
	public string Version => MajorVersion + "." + MinorVersion + Revision;
	public int ProtocolVersion => MajorVersion * 1000 + MinorVersion;
}