﻿namespace OnlyAModel.Protocol.Client.Models;

/// <summary>
/// Operations for <see cref="ClientMessageType.CharacterOperationRequest"/>.
/// </summary>
public enum CharacterOperation : byte
{
	Create = 0x01,
	Customize = 0x02,
	Delete = 0x03
}