﻿// ReSharper disable InconsistentNaming

namespace OnlyAModel.Protocol.Client.Models;

/// <summary>
/// Available game content.
/// </summary>
public readonly struct ClientContent
{
	// expansions
	private const int CLASSIC = 1;
	private const int SI = 2;
	private const int TOA = 3;
	private const int CAT = 4;
	private const int DR = 5;
	private const int LAB = 6;
	// addons
	private const int NEW_NEW_FRONTIERS = 0x20;
	private const int FOUNDATIONS = 0x40;
	private const int NEW_FRONTIERS = 0x80;
	// ReSharper disable once UnassignedReadonlyField - written by MemoryMarshal
	public readonly byte Expansions;
	public bool Classic => (Expansions & 0x0F) >= CLASSIC;
	public bool ShroudedIsles => (Expansions & 0x0F) >= SI;
	public bool TrialsOfAtlantis => (Expansions & 0x0F) >= TOA;
	public bool Catacombs => (Expansions & 0x0F) >= CAT;
	public bool DarknessRising => (Expansions & 0x0F) >= DR;
	public bool LabyrinthOfTheMinotaur => (Expansions & 0x0F) >= LAB;
	public bool NewNewFrontiers => (Expansions & NEW_NEW_FRONTIERS) != 0;
	public bool Foundations => (Expansions & FOUNDATIONS) != 0;
	public bool NewFrontiers => (Expansions & NEW_FRONTIERS) != 0;
}