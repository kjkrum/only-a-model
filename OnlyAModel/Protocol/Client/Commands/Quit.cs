﻿using OnlyAModel.Core;

namespace OnlyAModel.Protocol.Client.Commands;

public class Quit : ISlashCommand
{
	public IEnumerable<string> Aliases => new[] { "q", "quit" };
	
	public Task Invoke(string[] args, Session session)
	{
		session.ExitRegion();
		session.Send(new Protocol.Server.Messages.Quit(false));
		return Task.CompletedTask;
	}
}