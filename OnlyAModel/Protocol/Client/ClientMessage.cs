﻿using System.Buffers.Binary;

namespace OnlyAModel.Protocol.Client;

public readonly struct ClientMessage
{
	/// <summary>
	/// 10-byte header, 2-byte checksum.
	/// </summary>
	public const int EnvelopeBytes = 12;

	public readonly ReadOnlyMemory<byte> Bytes { get; }

	public ClientMessage(ReadOnlyMemory<byte> bytes)
	{
		Bytes = bytes;
		if(Checksum != ComputeChecksum())
		{
			throw new InvalidDataException("invalid checksum");
		}
	}

	public ClientMessageType Type => (ClientMessageType)Bytes.Span[9];

	public ushort Sequence => BinaryPrimitives.ReadUInt16BigEndian(Bytes.Span[2..]);

	public ReadOnlyMemory<byte> Payload => Bytes[10..^2];

	private ushort Checksum => BinaryPrimitives.ReadUInt16BigEndian(Bytes.Span[^2..]);

	private ushort ComputeChecksum()
	{
		var span = Bytes[..^2].Span;
		byte b0 = 0x7E;
		byte b1 = 0x7E;
		foreach (var b in span)
		{
			b0 += b;
			b1 += b0;
		}
		return (ushort)(b1 - (b0 + b1 << 8));
	}
}