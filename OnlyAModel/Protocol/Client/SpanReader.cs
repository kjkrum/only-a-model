﻿using System.Buffers.Binary;
using System.Text;

namespace OnlyAModel.Protocol.Client;

public ref struct SpanReader(ReadOnlySpan<byte> span)
{
	private readonly ReadOnlySpan<byte> _span = span;
	private int _pos;
	private ReadOnlySpan<byte> Span => _span[_pos..];

	public byte ReadByte() => _span[_pos++];
	
	/// <summary>
	/// Reads a string with a 32-bit length prefix and a redundant null
	/// terminator. The returned string does not include the prefix or
	/// terminator.
	/// </summary>
	public string ReadDAoCString()
	{
		var len = (int)ReadUInt32LE();
		// DOL assumes the client might send a 0-length string.
		// Since the length prefix includes the terminator, length 0
		// would seem to imply a null string with no terminator.
		// Let's see if the client actually does this...
		if (len == 0) throw new InvalidDataException("unexpected length 0");
		var s = Encoding.ASCII.GetString(Span[..(len - 1)]);
		_pos += len;
		return s;
	}

	// ReSharper disable once InconsistentNaming
	public float ReadFloatLE()
	{
		var f = BinaryPrimitives.ReadSingleLittleEndian(Span);
		_pos += sizeof(float);
		return f;
	}

	/// <summary>
	/// Reads a null-terminated string from a fixed-width field.
	/// </summary>
	public string ReadFixedString(int width)
	{
		var i = Span[..width].IndexOf((byte)0);
		if (i == -1) return "";
		var s = Encoding.ASCII.GetString(Span[..i]);
		_pos += width;
		return s;
	}

	/// <summary>
	/// Reads a C-style null-terminated string.
	/// </summary>
	public string ReadCString()
	{
		var i = Span.IndexOf((byte)0);
		if (i == -1) return "";
		var s = Encoding.ASCII.GetString(Span[..i]);
		_pos += s.Length;
		return s;
	}
	
	// ReSharper disable once InconsistentNaming
	public ushort ReadUInt16BE()
	{
		var u = BinaryPrimitives.ReadUInt16BigEndian(Span);
		_pos += sizeof(ushort);
		return u;
	}
	
	// ReSharper disable once InconsistentNaming
	public ushort ReadUInt16LE()
	{
		var u = BinaryPrimitives.ReadUInt16LittleEndian(Span);
		_pos += sizeof(ushort);
		return u;
	}

	// ReSharper disable once InconsistentNaming
	public uint ReadUInt32BE()
	{
		var u = BinaryPrimitives.ReadUInt32BigEndian(Span);
		_pos += sizeof(uint);
		return u;
	}
	
	// ReSharper disable once InconsistentNaming
	public uint ReadUInt32LE()
	{
		var u = BinaryPrimitives.ReadUInt32LittleEndian(Span);
		_pos += sizeof(uint);
		return u;
	}
	
	public void Skip(int bytes)
	{
		if (bytes < 0) throw new ArgumentException($"{nameof(bytes)} cannot be negative");
		_pos += bytes;
	}
}