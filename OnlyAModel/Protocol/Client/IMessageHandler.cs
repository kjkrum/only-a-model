﻿using OnlyAModel.Core;

namespace OnlyAModel.Protocol.Client;

/// <summary>
/// Handles messages of a specific <see cref="ClientMessageType"/>.
/// </summary>
public interface IMessageHandler
{
	ClientMessageType Type { get; }
	Task Handle(ClientMessage message, Session session);
}