﻿using OnlyAModel.Core;

namespace OnlyAModel.Protocol.Client;

public interface ISlashCommand
{
	/// <summary>
	/// Do not include the leading <c>'&amp;'</c>.
	/// </summary>
	public IEnumerable<string> Aliases { get; }

	/// <summary>
	/// The command alias is in <c>args[0]</c>.
	/// </summary>
	public Task Invoke(string[] args, Session session);
}