﻿namespace OnlyAModel.Protocol.Server.Messages;

public class Pong(uint timestamp, ushort sequence) : ServerMessage(ServerMessageType.Pong)
{
	protected override int WritePayload(Span<byte> span, int protocolVersion)
	{
		// LO PacketLib1124
		var writer = new SpanWriter(span);
		writer.WriteUInt32BE(timestamp); // LO has LE
		writer.Zero(4);
		writer.WriteUInt32BE(sequence);
		writer.Zero(6); // unknown
		return writer.Position;
	}
}