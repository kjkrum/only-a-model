﻿using OnlyAModel.Data.Enums;

namespace OnlyAModel.Protocol.Server.Messages;

public class PlayerRealm(Realm realm) : ServerMessage(ServerMessageType.PlayerRealm)
{
	protected override int WritePayload(Span<byte> span, int protocolVersion)
	{
		// LO PacketLib1125
		var writer = new SpanWriter(span);
		writer.WriteByte((byte)realm);
		writer.Zero(12); // unknown
		return writer.Position;
	}
}