﻿namespace OnlyAModel.Protocol.Server.Messages;

public class HandshakeResponse(string version, ushort build) : ServerMessage(ServerMessageType.HandshakeResponse)
{
	protected override int WritePayload(Span<byte> span, int _)
	{
		var writer = new SpanWriter(span);
		writer.WriteDAoCString(version);
		writer.WriteUInt16LE(build);
		return writer.Position;
	}
}