﻿namespace OnlyAModel.Protocol.Server.Messages;

public class BogusDisableSkills() : ServerMessage(ServerMessageType.DisableSkills)
{
	protected override int WritePayload(Span<byte> span, int protocolVersion)
	{
		span[0] = 0; // count of disabled skills
		return 1;
	}
}