﻿using System.Buffers.Binary;

namespace OnlyAModel.Protocol.Server.Messages;

public class DebugMode(bool debug) : ServerMessage(ServerMessageType.DebugMode)
{
	protected override int WritePayload(Span<byte> span, int protocolVersion)
	{
		BinaryPrimitives.WriteUInt16LittleEndian(span, (ushort)(debug ? 1 : 0));
		return sizeof(ushort);
	}
}