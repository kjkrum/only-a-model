﻿using OnlyAModel.Core.GameObjects;

namespace OnlyAModel.Protocol.Server.Messages;

public class PlayerPosition(PlayerCharacter pc) : ServerMessage(ServerMessageType.PlayerPosition)
{
	protected override int WritePayload(Span<byte> span, int protocolVersion)
	{
		// LO PacketLib1124
		var writer = new SpanWriter(span);
		writer.WriteFloatLE(pc.X);
		writer.WriteFloatLE(pc.Y);
		writer.WriteFloatLE(pc.Z);
		writer.WriteFloatLE(pc.Speed);
		writer.WriteFloatLE(pc.ZSpeed);
		writer.WriteUInt16BE(pc.SessionId);
		writer.WriteUInt16BE(pc.ZoneId);
		writer.WriteByte(pc.State); // LO does some bit shifting on state, writes it as uint16be
		writer.WriteByte(0);
		writer.WriteUInt16BE(0); // LO writes this as 2 bytes; DOL mentions something about steeds
		writer.WriteUInt16BE(pc.Heading);
		writer.WriteByte(pc.Action); // DOL and LO both construct a new value for action
		writer.WriteByte(0); // roleplay - not bit flags; only value 0x01 sets RP
		writer.WriteByte(0); // unknown
		writer.WriteByte((byte)(pc.HealthPercent + (pc.Attacking ? 0x80 : 0))); // attacking bit comes from DOL
		writer.WriteByte(pc.ManaPercent);
		writer.WriteByte(pc.EndurancePercent);
		return writer.Position;
	}
}