﻿namespace OnlyAModel.Protocol.Server.Messages;

public class CharacterRegion(ushort regionId, byte expansion) : ServerMessage(ServerMessageType.CharacterRegion)
{
	// LO conditionally sends ClientRegions (0x9E) instead of ClientRegion (0xB1)
	
	protected override int WritePayload(Span<byte> span, int protocolVersion)
	{
		var writer = new SpanWriter(span);
		// 0 expansion + blank ports & ip:
		// no delay, immediate CharacterSelectRequest, no WorldInitRequest
		// non-zero expansion + blank ports & ip: 20s delay
		// 0 expansion, valid ports, blank ip: 20s delay
		// 0 expansion, invalid ports ("-1"), blank ip: 20s delay
		// 0 expansion, blank ports, "0.0.0.0" ip: no delay, no init
		// 0 expansion, blank ports, "127.0.0.1" ip: no delay, no init
		// 0 expansion, "0" ports, "127.0.0.1" ip: no delay, no init
		// expansion, regionId, nothing else: 20s delay, init
		// 0 expansion, regionId, nothing else: no delay, no init
		// expansion, nothing else: 20s delay, init
		// 0 expansion, nothing else: no delay, no init
		// 255 expansion, nothing else: 20s delay, init
		// empty payload: no delay/no init on first click; delay/init on second click!
		
		// any non-zero value in the first byte OR any non-zero
		// port numbers causes the client to proceed after a delay.

		/*
		 * DOL notes say this should be the expansion as a byte followed by the
		 * region "skin" as a byte. Region is represented elsewhere as ushort.
		 * The "skin" is apparently related to instances. It seemingly
		 * identifies the map or model for the region. Perhaps multiple regions
		 * can share the same skin. Or maybe "skin" is the region containing
		 * the entrance to an instance.
		 *
		 * LO also has some notes about what this byte should be, but LO sends
		 * 0. Values in the 1124 logs were 10 or 13.
		 */
		writer.WriteByte(expansion); // expansion
		writer.WriteByte((byte)regionId);
		// live sent e.g. "region001" in the 1124 logs; LO just sends blank
		writer.Zero(20);
		// is this supposed to be the UDP port range?
		// TCPView shows that the client opens a random UDP port
		// but there is no attempt to connect to these ports
		writer.WriteFixedString("12345", 5); // LO calls this "from" port
		writer.WriteFixedString("12345", 5); // LO calls this "to" port
		writer.WriteFixedString("127.0.0.1", 20);
		
		// 1126 version
		// writer.WriteDAoCString("127.0.0.1");
		// writer.WriteUInt16BE(10400);
		// writer.Zero(2); // unknown
		// writer.WriteUInt16BE(10400);
		// writer.Zero(2); // unknown
		
		return writer.Position;
	}
}