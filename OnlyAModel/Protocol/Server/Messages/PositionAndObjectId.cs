﻿using OnlyAModel.Core;

namespace OnlyAModel.Protocol.Server.Messages;

public class PositionAndObjectId(GameObject gameObject) : ServerMessage(ServerMessageType.PositionAndObjectId)
{
	protected override int WritePayload(Span<byte> span, int protocolVersion)
	{
		// LO PacketLib1124
		var writer = new SpanWriter(span);
		writer.WriteFloatLE(gameObject.X);
		writer.WriteFloatLE(gameObject.Y);
		writer.WriteFloatLE(gameObject.Z);
		writer.WriteUInt16BE(gameObject.ObjectId);
		writer.WriteUInt16BE(gameObject.Heading);
		// TODO if zone is dungeon, LO sends the zone offsets
		// appears to be in db units, not multiplied by 8192
		// if not a dungeon, send 0
		writer.WriteUInt16BE(0); // zone x offset
		writer.WriteUInt16BE(0); // zone y offset
		writer.WriteUInt16BE(gameObject.RegionId);
		// TODO 0x80 = diving enabled; get from region
		// TODO LO adds 0x01 if diving
		writer.WriteByte(0x80);
		// TODO DoL sends server name as a pascal string if current region is housing; otherwise 0x00
		writer.WriteByte(0);
		writer.Zero(4); // unknown
		return writer.Position;
	}
}