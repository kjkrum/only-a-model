﻿using OnlyAModel.Data.Entities;

namespace OnlyAModel.Protocol.Server.Messages;

public class CharacterStatus(Character character) :
	ServerMessage(ServerMessageType.CharacterStatus)
{
	protected override int WritePayload(Span<byte> span, int protocolVersion)
	{
		// from DOL PacketLib168
		// writer.WriteByte(character.HealthPercent);
		// writer.WriteByte(character.ManaPercent);
		// var alive = (ushort)(character.HealthPercent > 0 ? 0x00 : 0x0F);
		// writer.WriteUInt16BE(alive);
		// writer.WriteByte((byte)(character.Sitting ? 0x02 : 0x00));
		// writer.WriteByte(character.EndurancePercent);
		// writer.WriteByte(character.ConcentrationPercent);
		// writer.WriteByte(0); // unknown

		// LO PacketLib1124
		var writer = new SpanWriter(span);
		writer.WriteByte(character.HealthPercent);
		writer.WriteByte(character.ManaPercent);
		writer.WriteByte((byte)(character.Sitting ? 0x02 : 0x00));
		writer.WriteByte(character.EndurancePercent);
		writer.WriteByte(character.ConcentrationPercent);
		writer.WriteByte(0); // unknown
		writer.WriteUInt16BE(character.MaxMana);
		writer.WriteUInt16BE(character.MaxEndurance);
		writer.WriteUInt16BE(character.MaxConcentration);
		writer.WriteUInt16BE(character.MaxHealth);
		writer.WriteUInt16BE(character.Health);
		writer.WriteUInt16BE(character.Endurance);
		writer.WriteUInt16BE(character.Mana);
		writer.WriteUInt16BE(character.Concentration);
		return writer.Position;

		
	}
}