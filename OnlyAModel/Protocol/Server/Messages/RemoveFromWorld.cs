﻿namespace OnlyAModel.Protocol.Server.Messages;

public class RemoveFromWorld(ushort objectId) : ServerMessage(ServerMessageType.RemoveFromWorld)
{
	protected override int WritePayload(Span<byte> span, int protocolVersion)
	{
		var writer = new SpanWriter(span);
		writer.WriteUInt16BE(objectId);
		// DOL sends player = 2, living NPC = 1, anything else = 0
		// maybe this controls whether the object sinks into the ground or vanishes
		// TODO try different values
		writer.WriteUInt16BE(2);
		return writer.Position;
	}
}