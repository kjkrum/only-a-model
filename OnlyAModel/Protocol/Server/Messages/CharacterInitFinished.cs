﻿namespace OnlyAModel.Protocol.Server.Messages;

public class CharacterInitFinished() : ServerMessage(ServerMessageType.CharacterInitFinished)
{
	protected override int WritePayload(Span<byte> span, int protocolVersion)
	{
		span[0] = 0;
		return sizeof(byte);
	}
}