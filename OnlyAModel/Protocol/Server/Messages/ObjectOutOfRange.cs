﻿namespace OnlyAModel.Protocol.Server.Messages;

public class ObjectOutOfRange(ushort objectId) : ServerMessage(ServerMessageType.ObjectOutOfRange)
{
	protected override int WritePayload(Span<byte> span, int protocolVersion)
	{
		var writer = new SpanWriter(span);
		writer.WriteUInt16BE(objectId);
		writer.WriteUInt16BE(1); // unknown
		return writer.Position;
	}
}