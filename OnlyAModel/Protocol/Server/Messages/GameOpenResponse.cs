﻿namespace OnlyAModel.Protocol.Server.Messages;

public class GameOpenResponse(byte confirmUdp) : // bool?
	ServerMessage(ServerMessageType.GameOpenResponse)
{
	protected override int WritePayload(Span<byte> span, int protocolVersion)
	{
		span[0] = confirmUdp;
		return sizeof(byte);
	}
}