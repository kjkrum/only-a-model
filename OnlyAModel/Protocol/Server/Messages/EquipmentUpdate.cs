﻿namespace OnlyAModel.Protocol.Server.Messages;

public class EquipmentUpdate(ushort objectId, float speed) : ServerMessage(ServerMessageType.EquipmentUpdate)
{
	protected override int WritePayload(Span<byte> span, int protocolVersion)
	{
		// see DOL PacketLib1124.cs SendLivingEquipmentUpdate
		var writer = new SpanWriter(span);
		writer.WriteUInt16BE(objectId);
		// TODO visible weapon slots
		writer.WriteByte(0);
		// DOL writes speed as a byte
		// sending a float doesn't seem to hurt
		// this might change when other bytes are non-zero
		writer.WriteFloatLE(speed);
		// TODO cloak and helm
		writer.WriteByte(0);
		// TODO hood and quiver
		writer.WriteByte(0);
		// TODO equipment
		writer.WriteByte(0);
		return writer.Position;
	}
}