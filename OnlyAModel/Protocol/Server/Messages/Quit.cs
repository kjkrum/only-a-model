﻿namespace OnlyAModel.Protocol.Server.Messages;

public class Quit(bool exitClient) : ServerMessage(ServerMessageType.Quit)
{
	protected override int WritePayload(Span<byte> span, int protocolVersion)
	{
		// LO PacketLib1124
		var writer = new SpanWriter(span);
		writer.WriteBool(exitClient);
		// LO sends level as byte
		return writer.Position;
	}
}