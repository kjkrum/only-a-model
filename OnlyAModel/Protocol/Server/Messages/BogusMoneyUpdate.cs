﻿namespace OnlyAModel.Protocol.Server.Messages;

public class BogusMoneyUpdate() : ServerMessage(ServerMessageType.MoneyUpdate)
{
	protected override int WritePayload(Span<byte> span, int protocolVersion)
	{
		var writer = new SpanWriter(span);
		writer.WriteByte(4); // copper
		writer.WriteByte(3); // silver
		writer.WriteUInt16BE(2); // gold
		writer.WriteUInt16BE(5); // mithril
		writer.WriteUInt16BE(1); // platinum
		return writer.Position;
	}
}