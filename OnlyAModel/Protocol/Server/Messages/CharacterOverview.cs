﻿using OnlyAModel.Data.Entities;
using OnlyAModel.Data.Enums;

namespace OnlyAModel.Protocol.Server.Messages;

public class CharacterOverview : ServerMessage
{
	private readonly Character?[] _characters = new Character[10];
	
	// Message type changed between 1125 and 1126 but the format remains almost the same.
	
	public CharacterOverview(IEnumerable<Character> characters, int protocolVersion)
		: base(protocolVersion <= 1125 ? ServerMessageType.CharacterOverview : ServerMessageType.CharacterOverview1126)
	{
		foreach (var character in characters)
		{
			var i = character.Slot - (10 * ((byte)character.Realm - 1));
			_characters[i] = character;
		}
	} 
	
	protected override int WritePayload(Span<byte> span, int protocolVersion)
	{
		var writer = new SpanWriter(span);
		if (protocolVersion <= 1125)
		{
			writer.Zero(8); // unknown	
		}
		else
		{
			writer.WriteByte(1); // unknown
			writer.Zero(15); // unknown
		}
		foreach (var character in _characters)
		{
			WriteCharacter(character, protocolVersion, ref writer);	
		}
		return writer.Position;
	}
	
	private static void WriteCharacter(Character? c, int protocolVersion, ref SpanWriter s)
		{
			if(c == null)
			{
				s.WriteByte(0);
				return;
			}
			s.WriteByte((byte)c.Level);
			s.WriteDAoCString(c.Name);
			if (protocolVersion <= 1125)
			{
				s.WriteByte(0x18); // unknown
				s.WriteUInt32BE(1); // unknown				
			}
			else
			{
				s.WriteUInt32LE(0x18); // unknown
				s.WriteByte(1); // unknown
			}
			s.WriteByte(c.EyeSize);
			s.WriteByte(c.LipSize);
			s.WriteByte(c.EyeColor);
			s.WriteByte(c.HairColor);
			s.WriteByte(c.FaceType);
			s.WriteByte(c.HairStyle);
			s.WriteByte((byte)((c.Boots << 4) | c.Gloves));
			s.WriteByte((byte)((c.Torso << 4) | (c.HoodUp ? 1 : 0)));
			s.WriteByte(c.CustomizationStep);
			s.WriteByte(c.Mood);
			s.Zero(13); // unknown
			s.WriteDAoCString(c.LocationDescription);
			s.WriteDAoCString(c.Class.ToString());
			s.WriteDAoCString(c.Race.ToString());
			s.WriteUInt16LE(c.Model);
			// DOL represents region as two separate bytes
			s.WriteUInt16LE(c.RegionId);

			s.WriteUInt16LE(c.HelmetModel);
			s.WriteUInt16LE(c.GlovesModel);
			s.WriteUInt16LE(c.BootsModel);
			s.WriteUInt16LE(c.MainhandColor);
			s.WriteUInt16LE(c.TorsoModel);
			s.WriteUInt16LE(c.CloakModel);
			s.WriteUInt16LE(c.LegsModel);
			s.WriteUInt16LE(c.ArmsModel);
			s.WriteUInt16LE(c.HelmetColor);
			s.WriteUInt16LE(c.GlovesColor);
			s.WriteUInt16LE(c.BootsColor);
			s.WriteUInt16LE(c.OffhandColor);
			s.WriteUInt16LE(c.TorsoColor);
			s.WriteUInt16LE(c.CloakColor);
			s.WriteUInt16LE(c.LegsColor);
			s.WriteUInt16LE(c.ArmsColor);
			s.WriteUInt16LE(c.MainhandModel);
			s.WriteUInt16LE(c.OffhandModel);
			s.WriteUInt16LE(c.TwoHandModel);
			s.WriteUInt16LE(c.RangedModel);

			s.WriteByte(c.Strength);
			s.WriteByte(c.Dexterity);
			s.WriteByte(c.Constitution);
			s.WriteByte(c.Quickness);
			s.WriteByte(c.Intelligence);
			s.WriteByte(c.Piety);
			s.WriteByte(c.Empathy);
			s.WriteByte(c.Charisma);
			s.WriteByte((byte)c.Class);
			s.WriteByte((byte)c.Realm);

			// DOL combines race and gender differently.
			s.WriteByte((byte)((byte)c.Race + (byte)c.Gender));

			switch (c.ActiveWeapon)
			{
				case ActiveWeapon.TwoHanded:
					s.WriteByte(2);
					s.WriteByte(2);
					break;
				case ActiveWeapon.Ranged:
					s.WriteByte(3);
					s.WriteByte(3);
					break;
				default:
					s.WriteByte((byte)(c.MainhandModel == 0 ? 0xFF : 0x00));
					s.WriteByte((byte)(c.OffhandModel == 0 ? 0xFF : 0x01));
					break;
			}
			s.WriteByte(1); // TODO get "in SI zone" from region
			s.WriteByte(c.Constitution); // no idea
		}
}