﻿using OnlyAModel.Protocol.Server.Models;

namespace OnlyAModel.Protocol.Server.Messages;

public class BogusInventoryUpdate(InventoryType type) : ServerMessage(ServerMessageType.InventoryUpdate)
{
	protected override int WritePayload(Span<byte> span, int protocolVersion)
	{
		// LO PacketLib1124
		var writer = new SpanWriter(span);
		writer.WriteByte(0); // item count
		writer.WriteByte(0); // 0x01 shield in left hand?
		writer.WriteByte(0); // 0x01 cloak invisible | 0x02 helm invisible
		// TODO next byte is different if type is house vault
		writer.WriteByte(0); // 0x01 hood up | active quiver slot
		writer.WriteByte(0); // visible active weapons?
		writer.WriteByte((byte)type);
		// TODO write items
		return writer.Position;
	}
}