﻿using OnlyAModel.Data.Entities;

namespace OnlyAModel.Protocol.Server.Messages;

public class CharacterPoints(Character character) :
	ServerMessage(ServerMessageType.CharacterPoints)
{
	protected override int WritePayload(Span<byte> span, int protocolVersion)
	{
		// LO PacketLib1124
		var writer = new SpanWriter(span);
		writer.WriteUInt32LE(character.RealmPoints);
		writer.WriteUInt16BE(character.ExperienceBubbles);
		writer.WriteUInt16BE(character.SpecPoints);
		writer.WriteUInt32LE(character.BountyPoints);
		writer.WriteUInt16BE(character.RealmSpecPoints);
		writer.WriteUInt16BE(character.ChampionBubbles);
		writer.WriteUInt64LE(character.Experience);
		writer.WriteUInt64LE(character.NextLevel);
		writer.WriteUInt64LE(character.ChampionExperience);
		writer.WriteUInt64LE(character.NextChampionLevel);
		return writer.Position;
	}
}