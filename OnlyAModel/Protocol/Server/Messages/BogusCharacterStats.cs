﻿namespace OnlyAModel.Protocol.Server.Messages;

public class BogusCharacterStats() : ServerMessage(ServerMessageType.CharacterStats)
{
	protected override int WritePayload(Span<byte> span, int protocolVersion)
	{
		// the same format is used for resists
		// there's an extra zero after each group of stats because there are 8 stats and 9 resists
		// 18 bytes - base stats as uint16be
		// 18 bytes - buffs as uint16be
		// 18 bytes - item bonuses as uint16be
		// 9 bytes - item caps
		// 9 bytes - RA bonuses
		// 1 byte stats/resists marker - 0x00 for stats, 0xFF for resists
		// 1 byte con loss on death - 0 for resists
		// 2 bytes max health as uint16be - 0 for resists
		// 2 bytes unknown
		var writer = new SpanWriter(span);
		writer.Zero(72);
		writer.WriteByte(0); // stats marker
		writer.WriteByte(0); // con loss
		writer.WriteUInt16BE(100); // max health
		writer.WriteUInt16BE(0); // unknown
		return writer.Position;
	}
}