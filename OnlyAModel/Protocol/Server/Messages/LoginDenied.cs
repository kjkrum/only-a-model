using OnlyAModel.Protocol.Server.Models;

namespace OnlyAModel.Protocol.Server.Messages;

internal class LoginDenied(LoginError error) : ServerMessage(ServerMessageType.LoginDenied)
{
	protected override int WritePayload(Span<byte> span, int _)
	{
		// LO PacketLib1124
		span[0] = (byte)error;
		// LO and DOL send additional stuff, but 1125-1128 don't seem to care
		return sizeof(byte);
	}
}