﻿using OnlyAModel.Core;

namespace OnlyAModel.Protocol.Server.Messages;

public class LoginGranted(string username, Config config) : ServerMessage(ServerMessageType.LoginGranted)
{
	protected override int WritePayload(Span<byte> span, int protocolVersion)
	{
		// LO PacketLib1125
		var writer = new SpanWriter(span);
		writer.WriteShortString(username);
		writer.WriteShortString(config.ServerName);
		writer.WriteByte((byte)config.ServerId);
		writer.WriteByte((byte)config.PvPMode);
		writer.WriteBool(false); // trial account
		return writer.Position;
	}
}