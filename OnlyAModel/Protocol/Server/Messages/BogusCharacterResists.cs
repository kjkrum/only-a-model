﻿namespace OnlyAModel.Protocol.Server.Messages;

public class BogusCharacterResists() : ServerMessage(ServerMessageType.CharacterStats)
{
	protected override int WritePayload(Span<byte> span, int protocolVersion)
	{
		// see BogusCharacterStats
		var writer = new SpanWriter(span);
		writer.Zero(72);
		writer.WriteByte(0xFF); // resists marker
		writer.Zero(5);
		return writer.Position;
	}
}