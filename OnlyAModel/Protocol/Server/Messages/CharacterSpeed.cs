﻿namespace OnlyAModel.Protocol.Server.Messages;

/// <summary>
/// Sets the character's walking speed.
/// </summary>
/// <param name="landSpeed">land speed as a percentage of the default</param>
/// <param name="waterSpeed">water speed as a percentage of land speed</param>
/// <param name="turningDisabled">true if the character cannot turn</param>
public class CharacterSpeed(ushort landSpeed, byte waterSpeed, bool turningDisabled) :
	ServerMessage(ServerMessageType.CharacterSpeed)
{
	protected override int WritePayload(Span<byte> span, int protocolVersion)
	{
		// LO PacketLib1124
		var writer = new SpanWriter(span);
		writer.WriteUInt16BE(landSpeed);
		writer.WriteBool(turningDisabled);
		writer.WriteByte(waterSpeed); // LO does some weird math
		return writer.Position;
	}
}