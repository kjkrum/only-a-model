﻿using OnlyAModel.Protocol.Server.Models;

namespace OnlyAModel.Protocol.Server.Messages;

public class NameCheckResponse(string name, NameStatus status) : ServerMessage(ServerMessageType.NameCheckResponse)
{
	protected override int WritePayload(Span<byte> span, int protocolVersion)
	{
		// LO PacketLib1124
		var writer = new SpanWriter(span);
		writer.WriteFixedString(name, 30);
		writer.Zero(24); // was username
		writer.WriteByte((byte)status);
		writer.Zero(3); // unknown
		return writer.Position;
	}
}