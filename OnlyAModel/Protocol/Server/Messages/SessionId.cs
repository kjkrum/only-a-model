﻿namespace OnlyAModel.Protocol.Server.Messages;

public class SessionId(ushort sessionId) : ServerMessage(ServerMessageType.SessionId)
{
	protected override int WritePayload(Span<byte> span, int protocolVersion)
	{
		// LO PacketLib1124
		var writer = new SpanWriter(span);
		writer.WriteUInt16LE(sessionId);
		return writer.Position;
	}
}