﻿namespace OnlyAModel.Protocol.Server.Messages;

public class Encumberance(ushort maxEncumberance, ushort encumberance) :
	ServerMessage(ServerMessageType.Encumberance)
{
	protected override int WritePayload(Span<byte> span, int protocolVersion)
	{
		// LO PacketLib1124
		var writer = new SpanWriter(span);
		writer.WriteUInt16BE(maxEncumberance);
		writer.WriteUInt16BE(encumberance);
		return writer.Position;
	}
}