﻿using OnlyAModel.Data.Entities;

namespace OnlyAModel.Protocol.Server.Messages;

public class BogusPlayerUpdate(Character character) : ServerMessage(ServerMessageType.VariousUpdate)
{
	protected override int WritePayload(Span<byte> span, int protocolVersion)
	{
		// LO PacketLib1124
		var writer = new SpanWriter(span);
		writer.WriteByte(0x03); // subcode
		writer.WriteByte(0x0f); // number of entry
		writer.WriteByte(0x00); // subtype
		writer.WriteByte(0x00); // unknown
		writer.WriteByte((byte)character.Level);
		writer.WriteShortString(character.Name);
		writer.WriteByte((byte)(character.MaxHealth >> 8)); // ???
		writer.WriteShortString(character.Class.ToString());
		writer.WriteByte((byte)(character.MaxHealth & 0xFF)); // ???				
		writer.WriteShortString(""); // profession?
		writer.WriteByte(0); // unknown
		writer.WriteShortString(""); // title
		writer.WriteByte(0); // realm rank
		writer.WriteShortString(""); // rr title
		writer.WriteByte(0); // realm skill points - should be ushort?
		writer.WriteShortString(character.Class.ToString());
		writer.WriteByte(0); // house high byte
		writer.WriteShortString(""); // guild name
		writer.WriteByte(0); // house low byte
		writer.WriteShortString(""); // last name
		writer.WriteByte(1); // master level + 1				
		writer.WriteShortString(character.Race.ToString());				
		writer.WriteByte(0); // unknown
		writer.WriteShortString(""); // guild rank title
		writer.WriteByte(0);
		writer.WriteShortString("None"); // craft skill
		writer.WriteByte(0);
		writer.WriteShortString(""); // crafter title
		writer.WriteByte(0);
		writer.WriteShortString(""); // ML title
		writer.WriteByte(0);
		writer.WriteShortString(""); // custom title
		writer.WriteByte(0); // champion level
		writer.WriteShortString(""); // champion title
		return writer.Position;
	}
}