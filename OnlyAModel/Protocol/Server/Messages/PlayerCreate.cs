﻿using OnlyAModel.Data.Entities;

namespace OnlyAModel.Protocol.Server.Messages;

public class PlayerCreate(ushort sessionId, ushort objectId, Character c) : ServerMessage(ServerMessageType.PlayerCreate)
{
	protected override int WritePayload(Span<byte> span, int protocolVersion)
	{
		var writer = new SpanWriter(span);
			writer.WriteFloatLE(c.X);
			writer.WriteFloatLE(c.Y);
			writer.WriteFloatLE(c.Z);
			writer.WriteUInt16BE(sessionId);
			writer.WriteUInt16BE(objectId);
			writer.WriteUInt16BE(c.Heading);
			writer.WriteUInt16BE(c.Model);
			writer.WriteByte((byte)c.Level);

			// DOL's GetLivingRealm includes a condition to make all players see admins as being in their own realm
			// this is a case of message contents depending on the recipient
			// TODO change IGameObject methods to accommodate this kind of thing?

			int flags = (byte)c.Realm << 2;
			// TODO if dead flags |= 0x01;
			// TODO if swimming flags |= 0x02;
			// TODO if stealthed flags |= 0x10;
			// TODO if wireframe flags |= 0x20;
			if (c.Class == Data.Enums.Class.Vampiir) flags |= 0x40; // flying
			writer.WriteByte((byte)flags);

			// DOL notes suggest that some character attributes are combined, such as eye size + nose size
			// hopefully we're supposed to send them the same way they were combined in character creation
			writer.WriteByte(c.EyeSize);
			writer.WriteByte(c.LipSize);
			writer.WriteByte(c.Mood);
			writer.WriteByte(c.EyeColor);
			writer.WriteByte(c.HairColor);
			writer.WriteByte(c.FaceType);
			writer.WriteByte(c.HairStyle);

			writer.Zero(3); // unknowns

			writer.WriteShortString(c.Name);
			writer.WriteShortString(""); // TODO guild name
			writer.WriteShortString(""); // TODO last name
			writer.WriteShortString(""); // TODO prefix name for RR 12/13
			writer.WriteShortString(""); // TODO title

			// TODO horse stuff
			//if (playerToCreate.IsOnHorse)
			//{
			//	pak.WriteByte(playerToCreate.ActiveHorse.ID);
			//	if (playerToCreate.ActiveHorse.BardingColor == 0 && playerToCreate.ActiveHorse.Barding != 0 && playerToCreate.Guild != null)
			//	{
			//		int newGuildBitMask = (playerToCreate.Guild.Emblem & 0x010000) >> 9;
			//		pak.WriteByte((byte)(playerToCreate.ActiveHorse.Barding | newGuildBitMask));
			//		pak.WriteShortLowEndian((ushort)playerToCreate.Guild.Emblem);
			//	}
			//	else
			//	{
			//		pak.WriteByte(playerToCreate.ActiveHorse.Barding);
			//		pak.WriteShort(playerToCreate.ActiveHorse.BardingColor);
			//	}
			//	pak.WriteByte(playerToCreate.ActiveHorse.Saddle);
			//	pak.WriteByte(playerToCreate.ActiveHorse.SaddleColor);
			//}
			//else
			//{
			//	pak.WriteByte(0); // trailing zero
				writer.WriteByte(0);
			//}

			return writer.Position;
	}
}