﻿using System.Buffers.Binary;
using System.Text;

namespace OnlyAModel.Protocol.Server;

public ref struct SpanWriter(Span<byte> span)
{
	private readonly Span<byte> _span = span;
	public int Position { get; private set; }
	private Span<byte> Span => _span[Position..];

	/// <summary>
	/// Writes a boolean as a byte.
	/// </summary>
	public void WriteBool(bool value)
	{
		WriteByte((byte)(value ? 1 : 0));
	}
	
	/// <summary>
	/// Writes a byte.
	/// </summary>
	/// <param name="value"></param>
	public void WriteByte(byte value)
	{
		Span[0] = value;
		Position++;
	}

	/// <summary>
	/// Writes a C-style null-terminated string.
	/// </summary>
	/// <param name="value"></param>
	public void WriteCString(string value)
	{
		WritePlainString(value);
		WriteByte(0);
	}
		
	/// <summary>
	/// Writes a string with a 32-bit length prefix (which is absurd
	/// because the entire message only has a 16-bit length prefix) and a
	/// redundant null terminator.
	/// </summary>
	public void WriteDAoCString(string value)
	{
		WriteUInt32LE((uint)value.Length + 1);
		WritePlainString(value);
		WriteByte(0);
	}
		
	/// <summary>
	/// Writes a string with a fixed field width.
	/// </summary>
	public void WriteFixedString(string value, int width)
	{
		// allows the string to use the entire field with no terminator.
		// not sure if that's what we want.
		if (value.Length > width)
		{
			throw new ArgumentException("string length exceeds field width");
		}
		WritePlainString(value);
		Zero(width - value.Length);
	}
	
	// ReSharper disable once InconsistentNaming
	internal void WriteFloatLE(float value)
	{
		// used for X/Y/Z coordinates, running speeds, and falling speeds
		// DoL notes suggest the protocol only started using floats in 1.124
		BinaryPrimitives.WriteSingleLittleEndian(Span, value);
		Position += sizeof(float);
	}
	
	private void WritePlainString(string value)
	{
		Encoding.ASCII.GetBytes(value, Span);
		Position += value.Length;
	}

	/// <summary>
	/// Writes a string with a single byte length prefix.
	/// </summary>
	public void WriteShortString(string value)
	{
		WriteByte((byte)value.Length);
		WritePlainString(value);
	}

	// ReSharper disable once InconsistentNaming
	public void WriteUInt16BE(ushort value)
	{
		BinaryPrimitives.WriteUInt16BigEndian(Span, value);
		Position += sizeof(ushort);
	}
		
	// ReSharper disable once InconsistentNaming
	public void WriteUInt16LE(ushort value)
	{
		BinaryPrimitives.WriteUInt16LittleEndian(Span, value);
		Position += sizeof(ushort);
	}

	// ReSharper disable once InconsistentNaming
	public void WriteUInt32BE(uint value)
	{
		BinaryPrimitives.WriteUInt32BigEndian(Span, value);
		Position += sizeof(uint);
	}
		
	// ReSharper disable once InconsistentNaming
	public void WriteUInt32LE(uint value)
	{
		BinaryPrimitives.WriteUInt32LittleEndian(Span, value);
		Position += sizeof(uint);
	}
	
	// ReSharper disable once InconsistentNaming
	public void WriteUInt64LE(ulong value)
	{
		BinaryPrimitives.WriteUInt64LittleEndian(Span, value);
		Position += sizeof(ulong);
	}

	public void Zero(int bytes)
	{
		Span[..bytes].Clear();
		Position += bytes;
	}


	//
	// public void WriteUInt16BigEndian(ushort value)
	// {
	// 	BinaryPrimitives.WriteUInt16BigEndian(Span, value);
	// 	Position += sizeof(ushort);
	// }
	//
	// public void WriteUInt32BigEndian(uint value)
	// {
	// 	BinaryPrimitives.WriteUInt32BigEndian(Span, value);
	// 	Position += sizeof(uint);
	// }
	//
	// public void WriteUInt64LittleEndian(ulong value)
	// {
	// 	BinaryPrimitives.WriteUInt64LittleEndian(Span, value);
	// 	Position += sizeof(ulong);
	// }
	//
	// public void WriteSpan(ReadOnlySpan<byte> span)
	// {
	// 	span.CopyTo(Span);
	// 	Position += span.Length;
	// }
	//
	// //public void Write<T>(T value) where T : unmanaged
	// //{
	// //	var len = Marshal.SizeOf(typeof(T));
	// //	MemoryMarshal.Write(_span.Slice(_position, len), ref value);
	// //	_position += len;
	// //}
	//
	// public void WriteFloat(float value)
	// {
	// 	// used for X/Y/Z coordinates, running speeds, and falling speeds
	// 	// DoL notes suggest the protocol only started using floats in 1.124
	// 	BitConverter.TryWriteBytes(Span, value);
	// 	Position += sizeof(float);
	// }
}