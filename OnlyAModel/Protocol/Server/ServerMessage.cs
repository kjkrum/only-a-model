﻿using System.Buffers.Binary;

namespace OnlyAModel.Protocol.Server;

public abstract class ServerMessage(ServerMessageType type)
{
	public ServerMessageType Type => type;
		
	public ReadOnlyMemory<byte> Write(Memory<byte> buffer, int protocolVersion)
	{
		var span = buffer.Span;
		span[2..][0] = (byte)type;
		var len = WritePayload(span[3..], protocolVersion);
		BinaryPrimitives.WriteUInt16BigEndian(span, (ushort)len);
		return buffer[..(len + 3)];
	}

	protected abstract int WritePayload(Span<byte> span, int protocolVersion);
}