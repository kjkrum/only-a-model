namespace OnlyAModel.Protocol.Server.Models;

internal enum LoginError : byte
{
	PasswordIncorrect = 0x01, // 1.125d: "Your Password is incorrect!"
	AccountInvalid = 0x02, // 1.125d: "Your Account is invalid!"
	AuthServerUnavailable = 0x03, // 1.125d: "The Authorization Server is Unavailable!"
	UnsupportedClientVersion = 0x05, // 1.125d: "Incorrect Camelot client version. Please try patching again."
	CannotAccessUserAccount = 0x06,
	AccountNotFound = 0x07, // 1.125d: "No Record For User Found!"
	AccountNoAccessAnyGame = 0x08,
	AccountNoAccessThisGame = 0x09,
	AccountClosed = 0x0A, // 1.125d: "Your account has been closed!"
	AccountAlreadyLoggedIn = 0x0B,
	TooManyPlayersLoggedIn = 0x0C,
	GameCurrentlyClosed = 0x0D,
	AccountAlreadyLoggedIntoOtherServer = 0x10,
	AccountIsInLogoutProcedure = 0x11,
	ExpansionPacketNotAllowed = 0x12,
	AccountIsBannedFromThisServerType = 0x13,
	CafeIsOutOfPlayingTime = 0x14,
	PersonalAccountIsOutOfTime = 0x15,
	CafesAccountIsSuspended = 0x16,
	NotAuthorizedToUseExpansionVersion = 0x17,
	ServiceNotAvailable = 0xAA
}