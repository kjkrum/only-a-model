﻿namespace OnlyAModel.Protocol.Server.Models;

public enum ChatLocation
{
	ChatWindow = 0x00,
	PopupWindow = 0x01,
	SystemWindow = 0x02
}