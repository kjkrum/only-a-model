﻿namespace OnlyAModel.Protocol.Server.Models;

public enum InventoryType
{
	/// <summary>
	/// Inventory update, no window.
	/// </summary>
	Update = 0x00,
	/// <summary>
	/// All equippable slots.
	/// </summary>
	Equipment = 0x01,
	/// <summary>
	/// All non-equippable slots.
	/// </summary>
	Inventory = 0x02,
	/// <summary>
	/// Player vault window.
	/// </summary>
	PlayerVault = 0x03,
	/// <summary>
	/// Housing vault window.
	/// </summary>
	HouseVault = 0x04,
	/// <summary>
	/// Consignment window in owner/seller mode.
	/// </summary>
	ConsignmentOwner = 0x05,
	/// <summary>
	/// Consignment window in buyer mode.
	/// </summary>
	ConsignmentViewer = 0x06,
	/// <summary>
	/// Horse bags.
	/// </summary>
	HorseBags = 0x07
}