﻿using System.Text;
using OnlyAModel.Protocol.Server.Models;

namespace OnlyAModel.Protocol.Server;

public class ChatMessage(string message, ChatType type, ChatLocation location = ChatLocation.ChatWindow) :
	ServerMessage(ServerMessageType.ChatMessage)
{
	protected override int WritePayload(Span<byte> span, int protocolVersion)
	{
		var writer = new SpanWriter(span);
		writer.Zero(4); // was sessionId (BE) and 2 bytes unknown
		writer.WriteByte((byte)type);
		writer.Zero(3); // unknown
		var sb = new StringBuilder();
		// ReSharper disable once SwitchStatementMissingSomeEnumCasesNoDefault
		switch (location)
		{
			case ChatLocation.ChatWindow:
				sb.Append("@@");
				break;
			case ChatLocation.PopupWindow:
				sb.Append("##");
				break;
		}
		sb.Append(message);
		writer.WriteCString(sb.ToString());
		return writer.Position;
	}
}