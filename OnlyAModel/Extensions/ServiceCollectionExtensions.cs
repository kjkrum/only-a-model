﻿using System.Net;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using OnlyAModel.Core;
using OnlyAModel.Data;
using OnlyAModel.Data.Services;
using OnlyAModel.Protocol.Client;

namespace OnlyAModel.Extensions;

public static class ServiceCollectionExtensions
{
	/// <summary>
	/// Adds core components of an OAM server.
	/// </summary>
	public static IServiceCollection AddOamCore(this IServiceCollection services)
	{
		services.AddSingleton<Server>();
		services.AddSingleton<Listener>();
		services.AddSingleton<Sender>();
		services.AddSingleton<SessionFactory>();
		services.AddTransient<Session>();
		services.AddSingleton<Dispatcher>();
		services.AddAll<IMessageHandler>();
		services.AddSingleton<RegionFactory>();
		services.AddTransient<Region>();
		services.AddSingleton<World>();
		services.AddAll<ISlashCommand>();
		return services;
	}

	/// <summary>
	/// Adds all concrete <see cref="T"/> in the assembly as singletons.
	/// </summary>
	/// <param name="services"></param>
	/// <typeparam name="T"></typeparam>
	private static void AddAll<T>(this IServiceCollection services)
	{
		foreach (var type in typeof(Server).Assembly.GetTypes().Where(type =>
			         type.IsAssignableTo(typeof(T)) &&
			         type is { IsClass: true, IsPublic: true, IsAbstract: false }))
		{
			services.AddSingleton(typeof(T), type);
		}
	}
	
	/// <summary>
	/// Configures OAM data services.
	/// </summary>
	public static IServiceCollection AddOamData(
		this IServiceCollection services,
		Action<DbContextOptionsBuilder> configure)
	{
		services.AddDbContextFactory<OamContext>(configure);
		services.AddSingleton<PlayerService>();
		services.AddSingleton<CharacterService>();
		services.AddSingleton<RegionService>();
		return services;
	}
	
	/// <summary>
	/// Adds <see cref="Config"/> bound to the specified config section.
	/// </summary>
	public static IServiceCollection AddOamConfig(this IServiceCollection services, string configSection)
	{
		services.AddOptions<Config>()
			.BindConfiguration(configSection)
			.ValidateDataAnnotations()
			.Validate(config => config.MinProtocolVersion <= config.MaxProtocolVersion)
			.Validate(config => config.Bindings.Any())
			.Validate(config => config.Bindings.All(binding => IPAddress.TryParse(binding.Address, out _)))
			.ValidateOnStart();
		services.AddSingleton<Config>(sp => sp.GetRequiredService<IOptions<Config>>().Value);
		return services;
	}
}