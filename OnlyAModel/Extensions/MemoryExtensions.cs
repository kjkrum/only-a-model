﻿using System.Text;

namespace OnlyAModel.Extensions;

public static class MemoryExtensions
{
	public static string HexDump(this ReadOnlyMemory<byte> bytes, int bytesPerLine = 16)
	{
		var sb = new StringBuilder();
		var span = bytes.Span;
		for (var line = 0; line < bytes.Length; line += bytesPerLine)
		{
			sb.Append($"{line:x4}: ");
			var len = Math.Min(bytes.Length - line, bytesPerLine);
			for (var i = line; i < line + len; ++i)
			{
				var b = span[i];
				sb.Append($"{b:x2} ");
			}
			sb.Append(' ', (bytesPerLine - len) * 3 + 1);
			for (var i = line; i < line + len; ++i)
			{
				var b = span[i];
				sb.Append(b is >= 32 and <= 127 ? (char)b : '.');
			}
			if (line + len < bytes.Length)
			{
				sb.AppendLine();	
			}
		}
		return sb.ToString();
	}
}