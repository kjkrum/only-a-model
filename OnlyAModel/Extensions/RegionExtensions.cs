﻿using OnlyAModel.Data.Entities;
using OnlyAModel.Spatial;

namespace OnlyAModel.Extensions;

public static class RegionExtensions
{
	/// <summary>
	/// World units per zone unit.
	/// </summary>
	private const int WorldFactor = 8192;
	
	public static Rectangle ComputeBounds(this Region regionInfo)
	{
		if (!regionInfo.Zones.Any()) throw new ArgumentException("Region has no zones.");
		var minX = regionInfo.Zones.Select(z => z.OffsetX).Min() * WorldFactor;
		var minY = regionInfo.Zones.Select(z => z.OffsetY).Min() * WorldFactor;
		var maxX = regionInfo.Zones.Select(z => z.OffsetX + z.Width).Max() * WorldFactor;
		var maxY = regionInfo.Zones.Select(z => z.OffsetY + z.Height).Max() * WorldFactor;
		return Rectangle.Create(minX, minY, maxX, maxY);
	}
}