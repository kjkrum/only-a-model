﻿using OnlyAModel.Protocol.Server.Models;

namespace OnlyAModel.Extensions;

internal static class PvPModeExtensions
{
	internal static bool AllowMultipleRealms(this PvPMode mode) =>
		mode is PvPMode.PvP or PvPMode.PvE or PvPMode.PvW;
}