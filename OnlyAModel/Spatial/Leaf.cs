﻿using System.Collections.Immutable;

// ReSharper disable ConvertIfStatementToReturnStatement

namespace OnlyAModel.Spatial;

public class Leaf<T> : INode<T>
{
	private readonly ImmutableList<Entry<T>> _entries;

	public IEnumerable<Entry<T>> Entries => _entries;

	public Leaf(Entry<T> entry) : this(ImmutableList.Create(entry)) { }

	private Leaf(ImmutableList<Entry<T>> entries) => _entries = entries;

	public INode<T> Add(Entry<T> entry) => new Leaf<T>(_entries.Add(entry));
	
	public INode<T>? Remove(Entry<T> entry)
	{
		var entries = _entries.Remove(entry);
		if (entries.IsEmpty) return null;
		if (entries == _entries) return this;
		return new Leaf<T>(entries);
	}

	public INode<T> Replace(Entry<T> entry0, Entry<T> entry1)
	{
		var entries = _entries.Remove(entry0);
		if (entries == _entries) return this;
		entries = entries.Add(entry1);
		return new Leaf<T>(entries);
	}

	public IEnumerable<Entry<T>> Query(Rectangle query) => _entries;
}