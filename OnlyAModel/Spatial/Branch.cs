﻿// ReSharper disable ConvertIfStatementToSwitchStatement
// ReSharper disable ConvertIfStatementToReturnStatement

namespace OnlyAModel.Spatial;

public class Branch<T> : INode<T>
{
	private readonly Rectangle _bounds;
	private readonly int _depth;
	private readonly int _maxDepth;
	private readonly INode<T>? _left;
	private readonly INode<T>? _right;

	public IEnumerable<Entry<T>> Entries =>
		_left != null && _right != null ?
			_left.Entries.Concat(_right.Entries) :
			_left != null ? _left.Entries : _right!.Entries;

	public Branch(Rectangle bounds, int depth, int maxDepth, Entry<T> entry)
	{
		_bounds = bounds;
		_depth = depth;
		_maxDepth = maxDepth;
		var leftBounds = bounds.Left;
		if (leftBounds.Contains(entry))
		{
			_left = _left.Add(entry, leftBounds, depth + 1, maxDepth);
			_right = null;
		}
		else
		{
			_left = null;
			_right = _right.Add(entry, bounds.Right, depth + 1, maxDepth);
		}
	}

	private Branch(Rectangle bounds, int depth, int maxDepth,
		INode<T>? left, INode<T>? right)
	{
		_bounds = bounds;
		_depth = depth;
		_maxDepth = maxDepth;
		_left = left;
		_right = right;
	}

	public INode<T> Add(Entry<T> entry)
	{
		var leftBounds = _bounds.Left;
		if (leftBounds.Contains(entry))
		{
			var left = _left.Add(entry, leftBounds, _depth + 1, _maxDepth);
			return new Branch<T>(_bounds, _depth, _maxDepth, left, _right);
		}
		var right = _right.Add(entry, _bounds.Right, _depth + 1, _maxDepth);
		return new Branch<T>(_bounds, _depth, _maxDepth, _left, right);
	}

	public INode<T>? Remove(Entry<T> entry)
	{
		if (_bounds.Left.Contains(entry))
		{
			if (_left == null) return this;
			var left = _left.Remove(entry);
			if (left == null && _right == null) return null;
			if (left == _left) return this;
			return new Branch<T>(_bounds, _depth, _maxDepth, left, _right);
		}
		if (_right == null) return this;
		var right = _right.Remove(entry);
		if (_left == null && right == null) return null;
		if (right == _right) return this;
		return new Branch<T>(_bounds, _depth, _maxDepth, _left, right);
	}

	public INode<T> Replace(Entry<T> entry0, Entry<T> entry1)
	{
		var leftBounds = _bounds.Left;
		if (leftBounds.Contains(entry0))
		{
			// remove left
			if (_left == null) return this;
			if (leftBounds.Contains(entry1))
			{
				// replace left
				var left = _left.Replace(entry0, entry1);
				if (left == _left) return this;
				return new Branch<T>(_bounds, _depth, _maxDepth, left, _right);
			}
			else
			{
				// remove left, add right
				var left = _left.Remove(entry0);
				if (left == _left) return this;
				var right = _right.Add(entry1, _bounds.Right, _depth + 1, _maxDepth);
				return new Branch<T>(_bounds, _depth, _maxDepth, left, right);	
			}
		}
		// remove right
		if (_right == null) return this;
		if (leftBounds.Contains(entry1))
		{
			// remove right, add left
			var right = _right.Remove(entry0);
			if (right == _right) return this;
			var left = _left.Add(entry1, leftBounds, _depth + 1, _maxDepth);
			return new Branch<T>(_bounds, _depth, _maxDepth, left, right);
		}
		else
		{
			// replace right
			var right = _right.Replace(entry0, entry1);
			if (right == _right) return this;
			return new Branch<T>(_bounds, _depth, _maxDepth, _left, right);
		}
	}
	
	// TODO see if query performance can be improved with a "fully contains" test

	public IEnumerable<Entry<T>> Query(Rectangle query)
	{
		var left = _left != null && _bounds.Left.Intersects(query);
		var right = _right != null && _bounds.Right.Intersects(query);
		if (left && right)
		{
			return _left!.Query(query).Concat(_right!.Query(query));
		}
		if (left)
		{
			return _left!.Query(query);
		}
		if (right)
		{
			return _right!.Query(query);	
		}
		return Enumerable.Empty<Entry<T>>();
	}
}