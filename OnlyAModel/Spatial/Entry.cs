﻿namespace OnlyAModel.Spatial;

public readonly record struct Entry<T>(float X, float Y, float Z, T Value)
{
	/// <summary>
	/// True if the XYZ distance from this entry to the specified point is less
	/// than the radius.
	/// </summary>
	public bool IsInSphere(float x, float y, float z, float radius)
	{
		var dx = x - X;
		var dy = y - Y;
		var dz = z - Z;
		return dx * dx + dy * dy + dz * dz < radius * radius;
	}

	/// <summary>
	/// True if the XYZ distance from this entry to the specified entry is less
	/// than the radius.
	/// </summary>
	public bool IsInSphere(Entry<T> entry, float radius) =>
		IsInSphere(entry.X, entry.Y, entry.Z, radius);
	
	/// <summary>
	/// True if the XY distance from this entry to the specified point is less
	/// than the radius.
	/// </summary>
	public bool IsInCircle(float x, float y, float radius)
	{
		var dx = x - X;
		var dy = y - Y;
		return dx * dx + dy * dy < radius * radius;
	}

	/// <summary>
	/// True if the XY distance from this entry to the specified entry is less
	/// than the radius.
	/// </summary>
	public bool IsInCircle(Entry<T> entry, float radius) =>
		IsInCircle(entry.X, entry.Y, radius);
};