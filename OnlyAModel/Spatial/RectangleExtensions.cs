﻿namespace OnlyAModel.Spatial;

public static class RectangleExtensions
{
	public static bool Contains<T>(this Rectangle rectangle, Entry<T> entry) =>
		rectangle.Contains(entry.X, entry.Y);
}