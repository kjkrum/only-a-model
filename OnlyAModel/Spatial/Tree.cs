﻿using System.Diagnostics.CodeAnalysis;
using Ardalis.GuardClauses;

namespace OnlyAModel.Spatial;

/// <summary>
/// A lock-free concurrent spatial index. Similar to a k&#8209;d tree, but we
/// only need range queries, not nearest-neighbor queries, so we don't care if
/// the tree is balanced. Stores data in three dimensions but only indexes two
/// dimensions because the game world is basically flat.
/// </summary>
public class Tree<T>
{
	// TODO consider exposing different types to minimize allocations and avoid leaking root nodes
	// https://stackoverflow.com/questions/18552669/memory-allocation-when-using-foreach-loops-in-c-sharp
	// https://stackoverflow.com/questions/5486654/allowing-iteration-without-generating-any-garbage
	
	// TODO consider defaulting to leaf nodes until they reach a certain capacity
	
	private readonly Rectangle _bounds;
	private readonly int _maxDepth;
	private INode<T>? _root;

	/// <summary>
	/// All entries in the tree.
	/// </summary>
	public IEnumerable<Entry<T>> Entries =>
		_root?.Entries ?? Enumerable.Empty<Entry<T>>();

	// ReSharper disable once ConvertToPrimaryConstructor
	public Tree(Rectangle bounds, int maxDepth)
	{
		_bounds = bounds;
		_maxDepth = Guard.Against.Negative(maxDepth);
	}

	/// <summary>
	/// Adds an entry to the tree and optionally queries the tree as it existed
	/// immediately after adding the entry. Thus, the result may include the
	/// added entry.
	/// </summary>
	/// <param name="entry">the entry to add</param>
	/// <param name="query">the query rectangle</param>
	/// <param name="result">the query result</param>
	/// <returns>true if the entry was added; otherwise false</returns>
	public bool TryAdd(Entry<T> entry, Rectangle? query,
		[NotNullWhen(true)] out IEnumerable<Entry<T>>? result)
	{
		if (!_bounds.Contains(entry))
		{
			result = null;
			return false;
		}
		INode<T>? n0;
		INode<T> n1;
		do
		{
			n0 = _root;
			n1 = n0.Add(entry, _bounds, 0, _maxDepth);
		} while (Interlocked.CompareExchange(ref _root, n1, n0) != n0);
		if (n0 == null || !query.HasValue)
		{
			result = Enumerable.Empty<Entry<T>>();
			return true;
		}
		result = n1.Query(query.Value);
		return true;
	}

	/// <summary>
	/// Removes an entry from the tree and optionally queries the tree as it
	/// existed immediately after removing the entry. Thus, the result will not
	/// include the removed entry.
	/// </summary>
	/// <param name="entry">the entry to remove</param>
	/// <param name="query">the query rectangle</param>
	/// <param name="result">the query result</param>
	/// <returns>true if the entry was removed; otherwise false</returns>
	public bool TryRemove(Entry<T> entry, Rectangle? query,
		[NotNullWhen(true)] out IEnumerable<Entry<T>>? result)
	{
		if (!_bounds.Contains(entry))
		{
			result = null;
			return false;
		}
		INode<T>? n0;
		INode<T>? n1;
		do
		{
			n0 = _root;
			if (n0 == null)
			{
				result = null;
				return false;
			}
			n1 = n0.Remove(entry);
			// ReSharper disable once InvertIf
			if (n1 == n0)
			{
				result = null;
				return false;
			}
		} while (Interlocked.CompareExchange(ref _root, n1, n0) != n0);
		if (n1 == null || !query.HasValue)
		{
			result = Enumerable.Empty<Entry<T>>();
			return true;
		}
		result = n1.Query(query.Value);
		return true;
	}

	/// <summary>
	/// Removes an entry from the tree, adds a different entry to the tree, and
	/// optionally queries the tree as it existed immediately after the
	/// replacement. Thus, the result will not include the removed entry but
	/// may include the added entry.
	/// <para/>
	/// Without a query, replace is only slightly faster than remove and add.
	/// The advantage may be the ability to merge queries. For example, when a
	/// game object moves, a single query rectangle could bound the detection
	/// radii around the old and new positions.
	/// </summary>
	/// <param name="entry0">the entry to remove</param>
	/// <param name="entry1">the entry to add</param>
	/// <param name="query">the query rectangle</param>
	/// <param name="result">the query result</param>
	/// <returns>true if the replacement was successful; otherwise
	/// false</returns>
	public bool TryReplace(Entry<T> entry0, Entry<T> entry1, Rectangle? query,
		[NotNullWhen(true)] out IEnumerable<Entry<T>>? result)
	{
		if (!_bounds.Contains(entry0) ||
		    !_bounds.Contains(entry1))
		{
			result = null;
			return false;
		}
		// Treat entry0 == entry1 as a successful update without actually
		// modifying the tree because the client sends PlayerPositionUpdate as
		// a keepalive. If other clients don't receive this pointless update,
		// they'll assume the player is linkdead and stop rendering them.
		// Returning true here means the Region will broadcast the update.
		if (entry0 == entry1)
		{
			result = query.HasValue ?
				Query(query.Value) :
				Enumerable.Empty<Entry<T>>();
			return true;
		}
		INode<T>? n0;
		INode<T> n1;
		do
		{
			n0 = _root;
			if (n0 == null)
			{
				result = null;
				return false;
			}
			n1 = n0.Replace(entry0, entry1);
			// ReSharper disable once InvertIf
			if (n1 == n0)
			{
				result = null;
				return false;
			}
		} while (Interlocked.CompareExchange(ref _root, n1, n0) != n0);
		if (!query.HasValue)
		{
			result = Enumerable.Empty<Entry<T>>();
			return true;
		}
		result = n1.Query(query.Value);
		return true;
	}
	
	/// <summary>
	/// Returns entries within the query rectangle. The result likely includes
	/// some entries outside the query rectangle because it includes all
	/// entries in leaf nodes that partially intersect the query rectangle.
	/// </summary>
	public IEnumerable<Entry<T>> Query(Rectangle query)
	{
		// The returned enumerable leaks _root. This is an intentional design
		// decision to minimize the size of eventual list or array allocations.
		// Other filters will be applied before enumeration.
		if (!_bounds.Intersects(query))
		{
			return Enumerable.Empty<Entry<T>>();
		}
		return _root?.Query(query) ?? Enumerable.Empty<Entry<T>>();
	}
}