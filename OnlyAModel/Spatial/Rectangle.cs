﻿using System.Text;
using Ardalis.GuardClauses;

namespace OnlyAModel.Spatial;

public readonly record struct Rectangle
{
	// ReSharper disable MemberCanBePrivate.Global
	public readonly float MinX;
	public readonly float MinY;
	public readonly float MaxX;
	public readonly float MaxY;
	// ReSharper restore MemberCanBePrivate.Global

	public float Width => MaxX - MinX;
	public float Height => MaxY - MinY;
	public float AspectRatio
	{
		get
		{
			var w = Width;
			var h = Height;
			return w > h ? w / h : h / w;
		}
	}

	/// <summary>
	/// Creates a rectangle. Arguments will be sorted. Degenerate rectangles
	/// are prohibited.
	/// </summary>
	public static Rectangle Create(float x0, float y0, float x1, float y1)
	{
		Guard.Against.Equality(x0, x1);
		Guard.Against.Equality(y0, y1);
		var minX = Math.Min(x0, x1);
		var minY = Math.Min(y0, y1);
		var maxX = Math.Max(x0, x1);
		var maxY = Math.Max(y0, y1);
		return new Rectangle(minX, minY, maxX, maxY);
	}
	
	/// <summary>
	/// Creates a rectangle bounding a circle. Degenerate rectangles are
	/// prohibited.
	/// </summary>
	public static Rectangle Create(float x, float y, float radius)
	{
		Guard.Against.NegativeOrZero(radius);
		var minX = x - radius;
		var minY = y - radius;
		var maxX = x + radius;
		var maxY = y + radius;
		return new Rectangle(minX, minY, maxX, maxY);
	}

	/// <summary>
	/// Creates a rectangle from known valid coordinates.
	/// </summary>
	private Rectangle(float minX, float minY, float maxX, float maxY)
	{
		MinX = minX;
		MinY = minY;
		MaxX = maxX;
		MaxY = maxY;
	}
	
	/// <summary>
	/// The X dimension.
	/// </summary>
	private float DimX => MaxX - MinX;
	
	/// <summary>
	/// The Y dimension.
	/// </summary>
	private float DimY => MaxY - MinY;
	
	/// <summary>
	/// True if this rectangle should be split in X; otherwise split in Y.
	/// </summary>
	private bool SplitX => DimX > DimY;
	
	/// <summary>
	/// The midpoint of the X dimension.
	/// </summary>
	private float MidX => (MinX + MaxX) / 2;
	
	/// <summary>
	/// The midpoint of the Y dimension.
	/// </summary>
	private float MidY => (MinY + MaxY) / 2;
	
	/// <summary>
	/// The low X or Y end of a rectangle. The split axis of a square is
	/// undefined.
	/// </summary>
	public Rectangle Left => SplitX ?
		new Rectangle(MinX, MinY, MidX, MaxY) :
		new Rectangle(MinX, MinY, MaxX, MidY);
	
	/// <summary>
	/// The high X or Y end of a rectangle. The split axis of a square is
	/// undefined.
	/// </summary>
	public Rectangle Right => SplitX ?
		new Rectangle(MidX, MinY, MaxX, MaxY) :
		new Rectangle(MinX, MidY, MaxX, MaxY);

	/// <summary>
	/// Tests whether this rectangle contains the specified point.
	/// </summary>
	/// <param name="x">the X coordinate</param>
	/// <param name="y">the Y coordinate</param>
	/// <returns>true if this rectangle contains the specified point; otherwise
	/// false</returns>
	public bool Contains(float x, float y) =>
		x >= MinX && x < MaxX &&
		y >= MinY && y < MaxY;

	/// <summary>
	/// Tests whether this rectangle intersects another. Adjacent rectangles do
	/// not intersect.
	/// </summary>
	/// <param name="other">the other rectangle</param>
	/// <returns>true if the rectangles intersect; otherwise false</returns>
	public bool Intersects(Rectangle other) =>
		other.MinX < MaxX && MinX < other.MaxX &&
		other.MinY < MaxY && MinY < other.MaxY;
	
	/// <summary>
	/// Prevents infinite recursion into <see cref="Left"/> and <see
	/// cref="Right"/> in the compiler-generated <see cref="ToString"/>.
	/// </summary>
	private bool PrintMembers(StringBuilder sb)
	{
		sb.Append($"MinX = {MinX}, MinY = {MinY}, MaxX = {MaxX}, MaxY = {MaxY}");
		return true;
	}
}