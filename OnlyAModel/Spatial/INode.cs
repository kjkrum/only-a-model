﻿namespace OnlyAModel.Spatial;

public interface INode<T>
{
	/// <summary>
	/// All entries in this node and its children.
	/// </summary>
	IEnumerable<Entry<T>> Entries { get; }
	
	/// <summary>
	/// The caller asserts that the entry is within this node's bounds.
	/// </summary>
	INode<T> Add(Entry<T> entry);

	/// <summary>
	/// The caller asserts that the entry is within this node's bounds.
	/// </summary>
	INode<T>? Remove(Entry<T> entry);

	/// <summary>
	/// The caller asserts that both entries are within this node's bounds.
	/// </summary>
	INode<T> Replace(Entry<T> entry0, Entry<T> entry1);

	/// <summary>
	/// The caller asserts that the query intersects this node's bounds.
	/// </summary>
	IEnumerable<Entry<T>> Query(Rectangle query);
}