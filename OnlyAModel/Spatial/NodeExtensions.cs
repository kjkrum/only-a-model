﻿namespace OnlyAModel.Spatial;

public static class NodeExtensions
{
	/// <summary>
	/// Helper that calls <see cref="INode{T}.Add"/> on the node if it exists,
	/// or creates a new <see cref="Branch{T}"/> or <see cref="Leaf{T}"/>
	/// depending on depth. The caller asserts that the entry is within this
	/// node's bounds.
	/// </summary>
	/// <param name="node">the node to add the entry to</param>
	/// <param name="entry">the entry to add</param>
	/// <param name="bounds">the bounds for a new node</param>
	/// <param name="depth">the depth for a new node</param>
	/// <param name="maxDepth">the max depth of the tree</param>
	/// <returns>the new or modified node</returns>
	public static INode<T> Add<T>(this INode<T>? node, Entry<T> entry,
		Rectangle bounds, int depth, int maxDepth)
	{
		if (node != null)
		{
			return node.Add(entry);
		}
		if (depth < maxDepth)
		{
			return new Branch<T>(bounds, depth, maxDepth, entry);
		}
		return new Leaf<T>(entry);
	}
}