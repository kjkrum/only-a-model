﻿namespace OnlyAModel.Util;

public class MemoryBuffer<T>(Memory<T> memory)
{
	public int Position { get; private set; }
	public int Count { get; private set; }

	public Memory<T> Data => memory[Position..(Position + Count)];
	public Memory<T> Free => memory[(Position + Count)..];

	public void Advance(int count)
	{
		if (count > Count) throw new InvalidOperationException();
		Position += count;
		Count -= count;
	}

	public void Append(int count)
	{
		if (Position + Count + count > memory.Length) throw new InvalidOperationException();
		Count += count;
	}

	public void Compact()
	{
		if (Count == 0) Position = 0;
		if (Position == 0) return;
		Data.CopyTo(memory);
		Position = 0;
	}
}