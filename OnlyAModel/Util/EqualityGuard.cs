﻿using System.Runtime.CompilerServices;

// ReSharper disable once CheckNamespace
namespace Ardalis.GuardClauses;

public static class EqualityGuard
{
	public static void Equality(this IGuardClause guardClause,
		float left, float right,
		[CallerArgumentExpression($"{nameof(left)}")] string? leftName = null,
		[CallerArgumentExpression($"{nameof(right)}")] string? rightName = null)
	{
		// ReSharper disable once CompareOfFloatsByEqualityOperator
		if (left == right)
		{
			throw new ArgumentException($"Required inputs {leftName} and {rightName} cannot be equal.");
		}		
	}
}