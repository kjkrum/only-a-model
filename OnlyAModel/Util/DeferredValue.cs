﻿namespace OnlyAModel.Util;

public sealed class DeferredValue<T> : IDisposable where T : struct
{
	private int _initialized;
	private T? _value;
	public T Value => _value ?? throw new InvalidOperationException("Not initialized.");
	public bool HasValue => _value.HasValue;

	public void Init(T value)
	{
		if (Interlocked.CompareExchange(ref _initialized, 1, 0) != 0)
			throw new InvalidOperationException("Already initialized.");
		_value = value;
	}

	public void Dispose()
	{
		if (_value is IDisposable d)
		{
			d.Dispose();
		}
	}
}