﻿using System.Collections;
using System.Diagnostics.CodeAnalysis;

namespace OnlyAModel.Util;

/// <summary>
/// Provides <see cref="ushort"/> IDs from <c>1</c> to
/// <see cref="ushort.MaxValue"/> inclusive.
/// </summary>
public class IdPool
{
	private readonly BitArray _assigned = new(ushort.MaxValue);
	private int _next;

	public bool TryRent([NotNullWhen(true)] out Token? token)
	{
		lock (_assigned)
		{
			for (int o = 0; o < ushort.MaxValue; ++o)
			{
				var i = (_next + o) % ushort.MaxValue;
				if (_assigned[i]) continue;
				_assigned[i] = true;
				_next = (i + 1) % ushort.MaxValue;
				token = new Token(_assigned, i);
				return true;
			}
		}
		token = null;
		return false;
	}

	public Token Rent() =>
		TryRent(out var token) ? token : throw new InvalidOperationException();

	public sealed class Token(BitArray assigned, int index) : IDisposable
	{
		private int _disposed;
		public ushort Id => (ushort)(index + 1);
		
		public void Dispose()
		{
			// ReSharper disable once InvertIf
			if (Interlocked.CompareExchange(ref _disposed, 1, 0) == 0)
			{
				lock (assigned)
				{
					assigned[index] = false;
				}				
			}
		}
	}
}