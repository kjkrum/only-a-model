﻿namespace OnlyAModel.Util;

public sealed class Deferred<T> : IDisposable where T : class
{
	private T? _value;
	public T Value => _value ?? throw new InvalidOperationException("Not initialized.");
	public bool HasValue => _value != null;

	public void Init(T value)
	{
		ArgumentNullException.ThrowIfNull(value);
		if (Interlocked.CompareExchange(ref _value, value, null) != null)
			throw new InvalidOperationException("Already initialized.");
		_value = value;
	}

	public void Dispose()
	{
		if (_value is IDisposable d)
		{
			d.Dispose();
		}
	}
}