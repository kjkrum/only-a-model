﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OnlyAModel.Data.Entities;

namespace OnlyAModel.Data.Services;

public class CharacterService(IDbContextFactory<OamContext> dbFactory, ILogger<CharacterService> log)
{
	public async Task<IEnumerable<Character>> GetCharactersAsync(int playerId, CancellationToken cancel)
	{
		await using var db = await dbFactory.CreateDbContextAsync(cancel);
		return await db.Characters.Where(c => c.PlayerId == playerId).ToListAsync(cancel);
	}

	public async Task<bool> NameExists(string name, CancellationToken cancel)
	{
		// default collation is case-insensitive
		await using var db = await dbFactory.CreateDbContextAsync(cancel);
		return await db.Characters.AnyAsync(c => c.Name == name, cancel);
	}
	
	public async Task<Character?> CreateCharacterAsync(Action<Character> initialize, CancellationToken cancel)
	{
		var character = new Character();
		initialize(character);
		try
		{
			await using var db = await dbFactory.CreateDbContextAsync(cancel);
			db.Characters.Add(character);
			await db.SaveChangesAsync(cancel);
			return character;
		}
		catch (OperationCanceledException)
		{
			throw;
		}
		catch (Exception e)
		{
			log.LogWarning(e, "Error creating character.");
			return null;
		}
	}

	public async Task<Character?> GetCharacterAsync(int playerId, byte slot, CancellationToken cancel)
	{
		await using var db = await dbFactory.CreateDbContextAsync(cancel);
		return await db.Characters.SingleOrDefaultAsync(c => c.PlayerId == playerId && c.Slot == slot, cancel);
	}

	public async Task DeleteCharacterAsync(int playerId, byte slot, CancellationToken cancel)
	{
		await using var db = await dbFactory.CreateDbContextAsync(cancel);
		await db.Characters.Where(o => o.PlayerId == playerId && o.Slot == slot).ExecuteDeleteAsync(cancel);
	}
}