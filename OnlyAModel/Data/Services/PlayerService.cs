﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OnlyAModel.Data.Entities;

namespace OnlyAModel.Data.Services;

public class PlayerService(IDbContextFactory<OamContext> dbFactory, ILogger<PlayerService> log)
{
	public async Task<Player?> GetPlayer(string username, CancellationToken cancel)
	{
		await using var db = await dbFactory.CreateDbContextAsync(cancel);
		return await db.Players.SingleOrDefaultAsync(p => p.Username == username, cancel);
	}

	public async Task<Player?> CreatePlayer(string username, string password, CancellationToken cancel)
	{
		var player = new Player { Username = username };
		player.SetPassword(password);
		try
		{
			await using var db = await dbFactory.CreateDbContextAsync(cancel);
			db.Players.Add(player);
			await db.SaveChangesAsync(cancel);
			return player;
		}
		catch (OperationCanceledException)
		{
			throw;
		}
		catch (Exception e)
		{
			log.LogWarning(e, "Error creating character.");
			return null;
		}
	}
}