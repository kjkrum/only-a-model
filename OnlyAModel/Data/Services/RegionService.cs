﻿using Microsoft.EntityFrameworkCore;
using OnlyAModel.Data.Entities;

namespace OnlyAModel.Data.Services;

public class RegionService(IDbContextFactory<OamContext> dbFactory)
{
	public async Task<Region?> GetRegionAsync(ushort regionId, CancellationToken cancel)
	{
		await using var db = await dbFactory.CreateDbContextAsync(cancel);
		return await db.Regions
			.Include(r => r.Zones)
			.SingleOrDefaultAsync(r => r.RegionId == regionId, cancel);
	}
}