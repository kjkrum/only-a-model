﻿using Microsoft.EntityFrameworkCore;
using OnlyAModel.Data.Entities;

namespace OnlyAModel.Data;

public class OamContext(DbContextOptions<OamContext> options) : DbContext(options)
{
	public virtual DbSet<Player> Players { get; init; }
	public virtual DbSet<Character> Characters { get; init; }
	public virtual DbSet<Region> Regions { get; init; }
	public virtual DbSet<Zone> Zones { get; init; }
	
	protected override void OnModelCreating(ModelBuilder builder)
	{
		builder.ApplyConfigurationsFromAssembly(typeof(OamContext).Assembly);
	}
}