﻿namespace OnlyAModel.Data.Enums;

public enum ActiveWeapon
{
	Melee,
	TwoHanded,
	Ranged
}