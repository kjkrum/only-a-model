﻿namespace OnlyAModel.Data.Enums;

public enum Gender : byte
{
	Male = 0x00,
	Female = 0x80
}