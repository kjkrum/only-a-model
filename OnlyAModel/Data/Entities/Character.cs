﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using OnlyAModel.Core;
using OnlyAModel.Data.Enums;

// ReSharper disable PropertyCanBeMadeInitOnly.Local

namespace OnlyAModel.Data.Entities;

public class Character : IPosition
{
	public int CharacterId { get; private init; }
	public int PlayerId { get; set; }
	[MinLength(1), MaxLength(20)]
	public string Name { get; set; }
	// Level should be a byte but Range doesn't work for bytes.
	// https://github.com/efcore/EFCore.CheckConstraints/issues/120
	[Range(1, 50)]
	public int Level { get; set; }
	public bool Sitting { get; set; }
	public bool HoodUp { get; set; }

	/* Location */
	public ushort RegionId { get; set; }
	public float X { get; set; }
	public float Y { get; set; }
	public float Z { get; set; }
	public ushort Heading { get; set; }

	// TODO DOL gets location description from location somehow
	[MaxLength(23)]
	public string LocationDescription { get; set; }

	/* Classification */
	public Realm Realm { get; set; }
	/// <summary>
	/// The client recognizes 30 character slots numbered 0-29.
	/// Albs are 0-9, Mids are 10-19, and Hibs are 20-29.
	/// </summary>
	public byte Slot { get; set; }
	public Class Class { get; set; }
	public Race Race { get; set; }
	public Gender Gender { get; set; }

	/* Appearance */
	public ushort Model { get; set; }
	public byte CustomMode { get; set; }
	public byte EyeSize { get; set; }
	public byte LipSize { get; set; }
	public byte EyeColor { get; set; }
	public byte HairColor { get; set; }
	public byte FaceType { get; set; }
	public byte HairStyle { get; set; }
	/// <summary>
	/// 4 bits usable.
	/// </summary>
	public byte Boots { get; set; }
	/// <summary>
	/// 4 bits usable.
	/// </summary>
	public byte Gloves { get; set; }
	/// <summary>
	/// 4 bits usable.
	/// </summary>
	public byte Torso { get; set; }
	// TODO make customization step an enum?
	public byte CustomizationStep { get; set; }
	public byte Mood { get; set; }

	/* Equipment */
	public ushort HelmetModel { get; set; }
	public ushort GlovesModel { get; set; }
	public ushort BootsModel { get; set; }
	public ushort MainhandColor { get; set; }
	public ushort TorsoModel { get; set; }
	public ushort CloakModel { get; set; }
	public ushort LegsModel { get; set; }
	public ushort ArmsModel { get; set; }
	public ushort HelmetColor { get; set; }
	public ushort GlovesColor { get; set; }
	public ushort BootsColor { get; set; }
	public ushort OffhandColor { get; set; }
	public ushort TorsoColor { get; set; }
	public ushort CloakColor { get; set; }
	public ushort LegsColor { get; set; }
	public ushort ArmsColor { get; set; }
	public ushort MainhandModel { get; set; }
	public ushort OffhandModel { get; set; }
	public ushort TwoHandModel { get; set; }
	public ushort RangedModel { get; set; }
	public ActiveWeapon ActiveWeapon { get; set; }

	/* Stats */
	public byte Strength { get; set; }
	public byte Dexterity { get; set; }
	public byte Constitution { get; set; }
	public byte Quickness { get; set; }
	public byte Intelligence { get; set; }
	public byte Piety { get; set; }
	public byte Empathy { get; set; }
	public byte Charisma { get; set; }

	/* Status */
	public ushort MaxMana { get; set; }
	public ushort MaxEndurance { get; set; }
	public ushort MaxConcentration { get; set; }
	public ushort MaxHealth { get; set; }
	public ushort Health { get; set; }
	public ushort Endurance { get; set; }
	public ushort Mana { get; set; }
	public ushort Concentration { get; set; }

	[NotMapped] public byte HealthPercent => (byte)(Health * 100 / MaxHealth);
	[NotMapped] public byte ManaPercent => (byte)(Mana * 100 / MaxMana);
	[NotMapped] public byte EndurancePercent => (byte)(Endurance * 100 / MaxEndurance);
	[NotMapped] public byte ConcentrationPercent => (byte)(Concentration * 100 / MaxConcentration);

	/* Points */
	public uint RealmPoints { get; set; }
	/// <summary>
	/// 0-999, 100 = 1 bub
	/// </summary>
	public ushort ExperienceBubbles { get; set; }
	public ushort SpecPoints { get; set; }
	public uint BountyPoints { get; set; }
	public ushort RealmSpecPoints { get; set; }
	/// <summary>
	/// 0-999, 100 = 1 bub
	/// </summary>
	public ushort ChampionBubbles { get; set; }
	public ulong Experience { get; set; }
	public ulong NextLevel { get; set; }
	public ulong ChampionExperience { get; set; }
	public ulong NextChampionLevel { get; set; }

	// [Timestamp]
	// public byte[] RowVersion { get; set; }

	// internal Character Clone()
	// {
	// 	var c = (Character)MemberwiseClone();
	// 	//c.RowVersion = new byte[RowVersion.Length];
	// 	//Array.Copy(RowVersion, c.RowVersion, RowVersion.Length);
	// 	return c;
	// }

	private class Config : IEntityTypeConfiguration<Character>
	{
		public void Configure(EntityTypeBuilder<Character> builder)
		{
			builder.HasOne<Player>().WithMany().HasForeignKey(c => c.PlayerId);
			builder.HasOne<Region>().WithMany().HasForeignKey(c => c.RegionId);
			// default utf8mb4 collation is case-insensitive
			builder.HasIndex(o => o.Name).IsUnique();
			builder.HasIndex(o => new { o.PlayerId, o.Slot }).IsUnique();
		}
	}
}