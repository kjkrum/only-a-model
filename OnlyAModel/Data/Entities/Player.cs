﻿using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

// ReSharper disable UnusedAutoPropertyAccessor.Local
// ReSharper disable PropertyCanBeMadeInitOnly.Global
// ReSharper disable PropertyCanBeMadeInitOnly.Local

namespace OnlyAModel.Data.Entities;

public class Player
{
	private const int SaltBytes = 8;
	private const int HashBytes = 32;
	
	public int PlayerId { get; private init; }
	[MinLength(1), MaxLength(20)]
	public string Username { get; set; }
	[MaxLength(SaltBytes)]
	public byte[] Salt { get; private set; }
	[MaxLength(HashBytes)]
	public byte[] Hash { get; private set; }
	public bool Inactive { get; set; }
	
	public bool HasPassword(string password) =>
		ComputeHash(password, Salt).SequenceEqual(Hash);

	public void SetPassword(string password)
	{
		Salt = RandomNumberGenerator.GetBytes(SaltBytes);
		Hash = ComputeHash(password, Salt);
	}

	private static byte[] ComputeHash(string password, byte[] salt)
	{
		return KeyDerivation.Pbkdf2(
			password,
			salt,
			KeyDerivationPrf.HMACSHA512,
			100_000,
			HashBytes);
	}

	private class Config : IEntityTypeConfiguration<Player>
	{
		public void Configure(EntityTypeBuilder<Player> builder)
		{
			builder.HasIndex(o => o.Username).IsUnique();
		}
	}
}