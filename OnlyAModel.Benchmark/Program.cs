﻿using BenchmarkDotNet.Running;

namespace OnlyAModel.Benchmark;

public class Program
{
	public static void Main(string[] args)
	{
		BenchmarkRunner.Run<TreeBenchmarks>();
	}
}