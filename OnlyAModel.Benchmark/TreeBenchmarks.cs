﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Order;
using OnlyAModel.Spatial;

namespace OnlyAModel.Benchmark;

[Orderer(SummaryOrderPolicy.FastestToSlowest)]
[MemoryDiagnoser]
public class TreeBenchmarks
{
	private const int SimulateSeconds = 10;

	// Albion is 376,832 x 475,136.
	// Awareness radius is 6,000.
	// Depth 10 gives a leaf size of 11,776.
	// Depth 10 on a unit square gives a leaf size of 0.03125.
	// 11,776 / 0.03125 is precisely 376,832.
	
	[Params(10)]
	public int TreeDepth { get; set; }
	
	[Params(500)]
	public int NumberOfPlayers { get; set; }
	
	/// <summary>
	/// 1.125d sends 10 updates per second when walking at the default speed.
	/// </summary>
	[Params(20)]
	public int UpdatesPerSecond { get; set; }

	// weak effect; <10% difference between 0.01f and 0.1f
	
	/// <summary>
	/// At default speed, the player moves about 39 units per update. Scaled
	/// that's around 0.0001. Going 10x higher has almost no effect.
	/// </summary>
	[Params(0.0001f)]
	public float MovementPerUpdate { get; set; }
	
	// Radius 0 reveals the time spent purely updating the tree.
	// Subtract this time to understand how other results scale.
	// Scaled awareness radius is about 0.016.
	[Params(0.0f, 0.02f, 0.1f)]
	public float QueryRadius { get; set; }

	private Tree<int> Tree { get; set; } = null!;
	private Entry<int>[] Entries { get; set; } = null!;

	[GlobalSetup]
	public void Init()
	{
		Tree = new Tree<int>(Rectangle.Create(0, 0, 1, 1), TreeDepth);
		Entries	= Enumerable.Range(0, NumberOfPlayers)
			.Select(id => new Entry<int>(
				Random.Shared.NextSingle(),
				Random.Shared.NextSingle(), 0, id))
			.ToArray();
		if (Entries.Any(e => !Tree.TryAdd(e, null, out _)))
		{
			throw new Exception("add failed");
		}
	}
	
	[Benchmark]
	public void SimulateMovement()
	{
		for (var m = 0; m < UpdatesPerSecond * SimulateSeconds; ++m)
		{
			for (var e = 0; e < Entries.Length; ++e)
			{
				var e0 = Entries[e];
				var x = e0.X;
				var y = e0.Y;
				// derive movement direction from id
				switch (e0.Value & 0b11)
				{
					case 0:
						x += MovementPerUpdate;
						break;
					case 1:
						x -= MovementPerUpdate;
						break;
					case 2:
						y += MovementPerUpdate;
						break;
					case 3:
						y -= MovementPerUpdate;
						break;
				}
				// wrap if we go out of bounds
				if (x < 0) x += 1;
				if (x >= 1) x -= 1;
				if (y < 0) y += 1;
				if (y >= 1) y -= 1;
				var e1 = new Entry<int>(x, y, 0, e0.Value);
				Rectangle? q = QueryRadius == 0 ? null :
					Rectangle.Create(e1.X, e1.Y, QueryRadius);
				if (Tree.TryReplace(e0, e1, q, out var result))
				{
					_ = result.Count();
				}
				else
				{
					throw new Exception("replace failed");
				}
				Entries[e] = e1;
			}	
		}
	}
}