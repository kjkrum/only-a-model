﻿using OnlyAModel.Spatial;

namespace OnlyAModel.Test.Spatial;

[TestClass]
public class TreeTests
{
	[TestMethod]
	public void TestAddReplaceRemove()
	{
		var n = 1_000_000;
		var tree = new Tree<int>(Rectangle.Create(0, 0, 1, 1), 8);
		var entries = Enumerable.Range(1, n)
			.Select(i => new Entry<int>(
				Random.Shared.NextSingle(),
				Random.Shared.NextSingle(), 0, i))
			.ToArray();
		// add
		foreach (var entry in entries)
		{
			Assert.IsTrue(tree.TryAdd(entry, null, out _));
		}
		Assert.AreEqual(n, tree.Entries.Count());
		// replace
		for (var i = 0; i < entries.Length; ++i)
		{
			var e0 = entries[i];
			var e1 = new Entry<int>(
				Random.Shared.NextSingle(),
				Random.Shared.NextSingle(), 0, e0.Value);
			Assert.IsTrue(tree.TryReplace(e0, e1, null, out _));
			// replace is only slightly faster than remove/add without a query
			// Assert.IsTrue(tree.TryRemove(e0, null, out _));
			// Assert.IsTrue(tree.TryAdd(e1, null, out _));
			entries[i] = e1;
		}
		Assert.AreEqual(n, tree.Entries.Count());
		// remove
		foreach (var entry in entries)
		{
			Assert.IsTrue(tree.TryRemove(entry, null, out _));
		}
		Assert.AreEqual(0, tree.Entries.Count());
	}

	[TestMethod]
	public void TestQuery()
	{
		// depth 2 so only 4 leaves
		var tree = new Tree<int>(Rectangle.Create(0, 0, 2, 2), 2);
		// 100 entries in each leaf
		var leaf0 = Enumerable.Range(1, 100)
			.Select(i => new Entry<int>(
				Random.Shared.NextSingle(),
				Random.Shared.NextSingle(),
				0, i)).ToList();
		var leaf1 = Enumerable.Range(1, 100)
			.Select(i => new Entry<int>(
				Random.Shared.NextSingle() + 1,
				Random.Shared.NextSingle(),
				0, i)).ToList();
		var leaf2 = Enumerable.Range(1, 100)
			.Select(i => new Entry<int>(
				Random.Shared.NextSingle(),
				Random.Shared.NextSingle() + 1,
				0, i)).ToList();
		var leaf3 = Enumerable.Range(1, 100)
			.Select(i => new Entry<int>(
				Random.Shared.NextSingle() + 1,
				Random.Shared.NextSingle() + 1,
				0, i)).ToList();
		foreach (var entry in leaf0.Concat(leaf1).Concat(leaf2).Concat(leaf3))
		{
			Assert.IsTrue(tree.TryAdd(entry, null, out _));
		}
		// queries that should return the entries from each leaf
		var q0 = Rectangle.Create(0.5f, 0.5f, 0.1f);
		Assert.IsTrue(tree.Query(q0).OrderBy(e => e.Value).SequenceEqual(leaf0));
		var q1 = Rectangle.Create(1.5f, 0.5f, 0.1f);
		Assert.IsTrue(tree.Query(q1).OrderBy(e => e.Value).SequenceEqual(leaf1));
		var q2 = Rectangle.Create(0.5f, 1.5f, 0.1f);
		Assert.IsTrue(tree.Query(q2).OrderBy(e => e.Value).SequenceEqual(leaf2));
		var q3 = Rectangle.Create(1.5f, 1.5f, 0.1f);
		Assert.IsTrue(tree.Query(q3).OrderBy(e => e.Value).SequenceEqual(leaf3));
		// query that should contain all entries
		var qA =  Rectangle.Create(1, 1, 0.1f);
		Assert.AreEqual(400, tree.Query(qA).Count());
	}
}