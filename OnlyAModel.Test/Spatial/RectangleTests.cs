﻿using OnlyAModel.Spatial;

namespace OnlyAModel.Test.Spatial;

[TestClass]
public class RectangleTests
{
	[TestMethod]
	public void DegenerateRectanglesAreProhibited()
	{
		Assert.ThrowsException<ArgumentException>(() => Rectangle.Create(0, 0, 0, 1));
		Assert.ThrowsException<ArgumentException>(() => Rectangle.Create(0, 0, 1, 0));
		Assert.ThrowsException<ArgumentException>(() => Rectangle.Create(1, 1, 0));
	}
	
	[TestMethod]
	public void RectangleContainsPointOnMinBound()
	{
		var r0 = Rectangle.Create(0, 0, 1, 1);
		Assert.IsTrue(r0.Contains(0, 0));
		Assert.IsTrue(r0.Contains(0, 0.5f));
		Assert.IsTrue(r0.Contains(0.5f, 0));
	}

	[TestMethod]
	public void RectangleDoesNotContainPointOnMaxBound()
	{
		var r0 = Rectangle.Create(0, 0, 1, 1);
		Assert.IsFalse(r0.Contains(0, 1));
		Assert.IsFalse(r0.Contains(1, 0));
		Assert.IsFalse(r0.Contains(1, 1));
	}
	
	[TestMethod]
	public void FullyContainedRectanglesIntersect()
	{
		var r0 = Rectangle.Create(0, 0, 4, 4);
		var r1 = Rectangle.Create(1, 1, 3, 3);
		Assert.IsTrue(r0.Intersects(r1));
		Assert.IsTrue(r1.Intersects(r0));
	}

	[TestMethod]
	public void PartlyOverlappingRectanglesIntersect()
	{
		var r0 = Rectangle.Create(0, 0, 3, 3);
		var r1 = Rectangle.Create(1, 1, 4, 4);
		Assert.IsTrue(r0.Intersects(r1));
		Assert.IsTrue(r1.Intersects(r0));
	}
	
	[TestMethod]
	public void AdjacentRectanglesDoNotIntersect()
	{
		var r0 = Rectangle.Create(0, 0, 1, 1);
		// adjacent in X
		var r1 = Rectangle.Create(1, 0, 2, 1);
		Assert.IsFalse(r0.Intersects(r1));
		Assert.IsFalse(r1.Intersects(r0));
		// adjacent in Y
		var r2 = Rectangle.Create(0, 1, 1, 2);
		Assert.IsFalse(r0.Intersects(r2));
		Assert.IsFalse(r2.Intersects(r0));
	}

	[TestMethod]
	public void RectangleRightAndLeftAreCorrect()
	{
		var r0 = Rectangle.Create(0, 0, 8, 12);
		Assert.AreEqual(r0.Left, Rectangle.Create(0, 0, 8, 6));
		Assert.AreEqual(r0.Right, Rectangle.Create(0, 6, 8, 12));
		Assert.AreEqual(r0.Left.Left, Rectangle.Create(0, 0, 4, 6));
		Assert.AreEqual(r0.Left.Right, Rectangle.Create(4, 0, 8, 6));
		Assert.AreEqual(r0.Right.Left, Rectangle.Create(0, 6, 4, 12));
		Assert.AreEqual(r0.Right.Right, Rectangle.Create(4, 6, 8, 12));
	}
}