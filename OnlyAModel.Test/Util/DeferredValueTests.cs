﻿using OnlyAModel.Util;

namespace OnlyAModel.Test.Util;

[TestClass]
public class DeferredValueTests
{
	[TestMethod]
	public void TestInitAndGet()
	{
		var d = new DeferredValue<int>();
		Assert.IsFalse(d.HasValue);
		d.Init(42);
		Assert.IsTrue(d.HasValue);
		Assert.AreEqual(42, d.Value);
	}

	[TestMethod]
	public void TestDoubleInit()
	{
		Assert.ThrowsException<InvalidOperationException>(() =>
		{
			var d = new DeferredValue<int>();
			d.Init(42);
			d.Init(666);
		});
	}

	[TestMethod]
	public void TestGetNoInit()
	{
		Assert.ThrowsException<InvalidOperationException>(() =>
		{
			var d = new DeferredValue<int>();
			_ = d.Value;
		});
	}
}