﻿using OnlyAModel.Util;

namespace OnlyAModel.Test.Util;

[TestClass]
public class DeferredTests
{
	[TestMethod]
	public void TestInitAndGet()
	{
		var d = new Deferred<string>();
		Assert.IsFalse(d.HasValue);
		d.Init("fnord");
		Assert.IsTrue(d.HasValue);
		Assert.AreEqual("fnord", d.Value);
	}

	[TestMethod]
	public void TestDoubleInit()
	{
		Assert.ThrowsException<InvalidOperationException>(() =>
		{
			var d = new Deferred<string>();
			d.Init("fnord");
			d.Init("derp");
		});
	}

	[TestMethod]
	public void TestGetNoInit()
	{
		Assert.ThrowsException<InvalidOperationException>(() =>
		{
			var d = new Deferred<string>();
			_ = d.Value;
		});
	}
}