# Developer Notes

## Region Delay
When you click "Play" on the character selection screen, the client and server exchange the following messages (from the client's perspective):

```
SEND: 0x9D RegionRequest
RECV: 0xB1 CharacterRegion
SEND: 0xBF GameOpenRequest
RECV: 0x2D GameOpenResponse
SEND: 0xD4 WorldInitRequest
...
```

After receiving `0x2D`, the client hangs for about 20 seconds with the character animation frozen before sending `0xD4`. The `0xB1` identifies server's UDP port range. If the `0xB1` is bogus, there's no delay after the `0x2D` but the client never sends the `0xD4`. I suspect that the delay is related to UDP.

## UDP
TCPView shows that the client opens a random UDP port, but it never sends `UdpInitRequest (0x14)` or connects to the address and port specified in the `CharacterRegion (0xB1)`. Comments in DOL say the client will not use UDP unless it's using RC4.

## RC4
Portal's `connect.exe` can start the client with RC4 enabled. I got RC4 working in OAM by adapting [TheKroko's Uthgard code](https://github.com/thekroko/uthgard-opensource). However, the client still does not use UDP. Since the whole point of using RC4 was to get the client to use UDP and hopefully eliminate the 20s delay, I'm leaving these changes in the `rc4` branch for now. The Uthgard code operates on `byte[]`, so before it can be merged it should be rewritten to operate on `Memory<byte>` or `Span<byte>`.

## Protocol Versions
OAM was intended to support multiple client versions. That's why `ServerMessage.WritePayload` receives the protocol version. I originally thought that the major + minor version (e.g., 1125) identified the message format and the revision (e.g., 'd') identified changes that did not affect the message format. But if the comments in DOL are correct then this theory was wrong, because there *were* some format changes between the various 1125 revisions. This will become an issue if I ever get OAM working with a client version other than 1.125d. The client versions I have for testing include 1.125d, 1.126b, 1.127b/c/d/e, 1.128a/b/c, and 1.129a.

## Client 1.126+
With my launcher or Portal 2.9.1, client 1.126+ crashes when it receives `LoginGranted`. With Portal 2.9.4, I can get to the character creation screen but the buttons for race, class, and stats are missing and the client sends a bogus character creation message if I continue.

## Mystery Messages
The client sends various packets that neither vanilla DOL nor the Los Ojos fork have in their message type enums. I've named these messages `UnknownXX`, where "XX" is the message type byte in hex. I was hoping to figure out what some of these were by studying old logs, but the live servers didn't respond to them, either. They may have triggered state changes on the server side that aren't relevant for OAM.

## Client Launcher
Portal 2.9.1 was contemporary with client 1.125, and how it worked was publicly known. I based [my own launcher](https://gitlab.com/kjkrum/daoc-client-launcher) on how Portal 2.9.1 worked. Portal 2.9.4 supports at least up to client version 1.128, but how it works is not publicly known. Whatever Portal 2.9.4 does seems more correct, because it causes the client to send a second `0xF4` containing the RC4 key. I originally thought this was bogus, but old logs show that the client also sent this second `0xF4` when connecting to the live servers. So my launcher is abandoned and I'm sticking with Portal 2.9.4.

## Message Formats
`0xF4` is not the only message with multiple formats. Old logs show that the live servers sent `LoginGranted` with two slightly different formats within the same session. What the second format contains is a mystery, but it might be important.

## What's Missing?
Pretty much everything. You can log in and run around with other players, but there's no equipment, no mobs, and nothing is persistent after character creation. You can't open doors or travel between regions. You can't chat and there's nothing preventing the same account from being logged in more than once. I spent a long time trying to get UDP working and eliminate the region delay, but I'm putting that aside and moving on.