using OnlyAModel.Core;

namespace OnlyAModel.HostedService;

public class Worker(Server server) : BackgroundService
{
	protected override async Task ExecuteAsync(CancellationToken execCancel)
	{
		await server.RunAsync(execCancel)
			.ConfigureAwait(ConfigureAwaitOptions.SuppressThrowing);
		await Task.Delay(Timeout.Infinite, execCancel)
			.ConfigureAwait(ConfigureAwaitOptions.SuppressThrowing);
	}
}