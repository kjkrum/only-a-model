using Microsoft.EntityFrameworkCore;
using OnlyAModel.Extensions;
using Serilog;

namespace OnlyAModel.HostedService;

public static class Program
{
	public static void Main(string[] args)
	{
		var builder = Host.CreateDefaultBuilder(args);
		builder.UseSerilog((context, config) =>
		{
			config.ReadFrom.Configuration(context.Configuration);
		});
		builder.ConfigureServices((context, services) =>
		{
			services.AddOamCore();
			services.AddOamData(options =>
			{
				var connectionString = context.Configuration["Database:ConnectionString"];
				var serverVersion = ServerVersion.Parse(context.Configuration["Database:MySqlVersion"]);
				options.UseMySql(connectionString, serverVersion, mySqlOptions =>
					mySqlOptions.MigrationsAssembly(typeof(Program).Assembly));
			});
			services.AddOamConfig("OnlyAModel");
			services.AddWindowsService(options => options.ServiceName = "Only A Model");
			services.AddHostedService<Worker>();
		});
		var host = builder.Build();
		host.Run();
	}
}