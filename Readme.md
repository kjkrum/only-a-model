# Camelot! Camelot! Camelot!
It's Only A Model!&trade;

Only A Model!&trade; (OAM) is a game server for the greatest MMORPG of all time, [Dark Age of Camelot](https://darkageofcamelot.com/) (DAoC). OAM does not attempt to replicate any of the content from the official DAoC servers. OAM is more like a toolkit for experimenting with the DAoC client and creating your own world with customized content and mechanics.

Although OAM shares no code with [Dawn of Light](https://github.com/Dawn-of-Light/DOLSharp) (DOL), it's still a derivative work because I studied DOL extensively when writing OAM. Therefore, OAM is also licensed under the GPL. A huge amount of credit goes to whoever reverse engineered the DAoC message protocol for DOL.

## How To Play
Open the solution in your favorite .NET IDE and run the `OnlyAModel.HostedService` project. Use the [DAoC Portal](https://dolserver.sourceforge.net/) UI or Portal's `connect.exe` on the command line to direct the DAoC client to your private server. OAM's default TCP binding is `127.0.0.1:13013`.

The recommendation with DOL is to replace the client's `game.dll` with an older version. In contrast, I'm writing and testing OAM using completely stock DAoC clients. I designed OAM to support multiple client versions, but I've only been successful with client 1.125d. Unfortunately, I cannot legally distribute old client versions.

## Development Status
See the [developer notes](Notes.md).

## Git
I recommend putting your DAoC folder in a Git repository and committing whenever a patch comes out. Then you can easily switch between client versions by checking out different commits. Use this `.gitignore`:

```
logs/
*.myp
*.log
errorlog.txt
```

## Performance Tuning
A fundamental challenge for any game server is that the amount of upstream network traffic scales with the square of the number of players within detection radius of each other. OAM does what it can to minimize upstream traffic, but what it sends is largely dictated by the client. Even if OAM could somehow reduce upstream traffic by half, the gain would be erased by adding a few more players.

Home internet service is usually optimized for activities like web browsing where the downstream traffic greatly exceeds upstream traffic. If you're saturating your uplink, there's not much you can do except upgrade to commercial internet service.

Network drivers may also have default settings optimized for home use. For example, they typically have more receive buffers than send buffers. These and many other driver settings are described in Microsoft's [Performance in Network Adapters](https://learn.microsoft.com/en-us/windows-hardware/drivers/network/performance-in-network-adapters) tutorial.

But let's be real. If you only have a few players, none of this matters.